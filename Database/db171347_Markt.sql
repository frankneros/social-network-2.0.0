-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: internal-db.s171347.gridserver.com
-- Generation Time: Sep 27, 2014 at 09:24 AM
-- Server version: 5.1.63-rel13.4
-- PHP Version: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db171347_Markt`
--

-- --------------------------------------------------------

--
-- Table structure for table `Comments`
--

CREATE TABLE IF NOT EXISTS `Comments` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `postID` int(255) NOT NULL,
  `userID` int(255) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Friends`
--

CREATE TABLE IF NOT EXISTS `Friends` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `myID` int(255) NOT NULL,
  `friendID` int(255) NOT NULL,
  `theDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Hashtags`
--

CREATE TABLE IF NOT EXISTS `Hashtags` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `userID` int(255) NOT NULL,
  `postID` int(255) NOT NULL,
  `hashtag` varchar(255) NOT NULL,
  `theDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Likes`
--

CREATE TABLE IF NOT EXISTS `Likes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `likedPostID` int(255) NOT NULL,
  `userID` int(255) NOT NULL,
  `theDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Posts`
--

CREATE TABLE IF NOT EXISTS `Posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(255) NOT NULL,
  `postURL` longtext NOT NULL,
  `description` mediumtext NOT NULL,
  `likes` int(255) NOT NULL,
  `comments` int(255) NOT NULL,
  `theDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isVideo` varchar(255) NOT NULL,
  `isImage` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Reported`
--

CREATE TABLE IF NOT EXISTS `Reported` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `userID` int(255) NOT NULL,
  `postID` int(255) NOT NULL,
  `theDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first` varchar(355) NOT NULL,
  `last` varchar(355) NOT NULL,
  `username` varchar(355) NOT NULL,
  `password` varchar(355) NOT NULL,
  `email` varchar(355) NOT NULL,
  `picture` varchar(355) NOT NULL,
  `posts` int(255) NOT NULL,
  `followers` int(255) NOT NULL,
  `following` int(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `theDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
