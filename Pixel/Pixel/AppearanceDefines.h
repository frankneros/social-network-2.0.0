//
//  AppearanceDefines.h
//  Pixel
//
//  Created by Eric Partyka on 9/26/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#ifndef Pixel_AppearanceDefines_h
#define Pixel_AppearanceDefines_h

// Fonts
#define AppLightFontOfSize(__size__)            [UIFont fontWithName:@"Helvetica-Light" size:(__size__)]
#define AppFontOfSize(__size__)                 [UIFont fontWithName:@"Helvetica" size:(__size__)]

#endif
