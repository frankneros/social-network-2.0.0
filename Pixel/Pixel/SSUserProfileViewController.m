//
//  SSUserProfileViewController.m
//  SocialSell
//
//  Created by Eric Partyka on 8/2/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SSUserProfileViewController.h"
#import "SSUserProfileCell.h"
#import "SSUserProfileCollectionViewCell.h"
#import "AFNetworking.h"
#import "NSDate+NVTimeAgo.h"
#import "UIViewController+ScrollingNavbar.h"
#import "SSUserProfileCollectionCell.h"
#import "Settings.h"
#import "MainViewController.h"
#import "Settings.h"
#import "SettingsDefines.h"

@interface SSUserProfileViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

#pragma mark - Properties
@property (weak, nonatomic) IBOutlet UITableView *theTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *theCollectionView;
@property (strong, nonatomic) IBOutlet UIView *theBackgroundView;

@end

@implementation SSUserProfileViewController
{
    NSArray *theArray;
    NSArray *profileArray;
    NSUserDefaults *userDefaults;
    BOOL gettingData;
    BOOL isFriend;
    NSArray *jsonArray;
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidDisappear:(BOOL)animated
{
    [userDefaults setObject:NULL forKey:kFollowingNew];
    [userDefaults setObject:NULL forKey:kFollowersNew];
    [userDefaults setObject:NULL forKey:kTempUserID];
    [userDefaults setObject:@"0" forKey:kDidFriend];
    [userDefaults synchronize];
    
    [super viewDidDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;

    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:NULL forKey:kFollowingNew];
    [userDefaults setObject:NULL forKey:kFollowersNew];
    [userDefaults synchronize];
    
    NSLog(@"WWWWWWW %@", [userDefaults objectForKey:kTempUserID]);
    
    if ([userDefaults objectForKey:kTempUserID] != NULL)
    {
        if (![[userDefaults objectForKey:kTempUserID] isEqualToString:[userDefaults objectForKey:kMyID]])
        {
            gettingData = YES;
            [APINetworkController getMyPosts:[userDefaults objectForKey:kTempUserID]];
            [APINetworkController getMyFriend:[userDefaults objectForKey:kMyID] friendID:[userDefaults objectForKey:kTempUserID]];
        }
        else
        {
            [APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
        }

    }
    else
    {
        [APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
    }
    
    self.theBackgroundView.hidden = YES;
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 20)];
    view.backgroundColor = [UIColor colorWithRed:83.0/255.0 green:137.0/255.0 blue:210.0/255.0 alpha:1];
    [self.view addSubview:view];
    
    if (IS_IPHONE5) {
        self.theCollectionView.scrollEnabled = NO;
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.translucent = NO;
    
    if (!theArray) {
        theArray = [[NSArray alloc] init];
    }

    
    [self configureNavigationBar];
    [self getNotifications];
    [self configureView];
    
    [_theCollectionView registerNib:[UINib nibWithNibName:@"SSUserProfileCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"SSUserProfileCollectionCell"];
    [_theCollectionView registerNib:[UINib nibWithNibName:@"SSUserProfileCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"SSUserProfileCollectionViewCell"];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavigationBar
{
    
    self.navigationItem.title = @"User Profile";
}

- (void)configureView
{
    _theTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"CheckedFriend"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GotNewPosts"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GetProfilePosts"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshSearchVC"
                                               object:nil];
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"CheckedFriend"])
    {
        NSDictionary *userInfo = notification.userInfo;
        NSData *friends = [userInfo objectForKey:@"CheckedFriend"];
        NSError *e = nil;
        jsonArray = [NSJSONSerialization JSONObjectWithData:friends options: NSJSONReadingMutableContainers error: &e];
        
        if (!jsonArray) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            NSLog(@"ZZZZZZZZZZ %@", jsonArray);
        }
        
        if ([jsonArray count] > 0)
        {
            [userDefaults setObject:@"1" forKey:kDidFriend];
            [userDefaults synchronize];
        }
        else
        {
            [userDefaults setObject:@"0" forKey:kDidFriend];
            [userDefaults synchronize];

        }
        
        [self.theCollectionView reloadData];
        
    }
    
    if ([[notification name] isEqualToString:@"GotNewPosts"])
    {
        NSDictionary *userInfo = notification.userInfo;
        theArray = [userInfo objectForKey:@"newPosts"];
    
        
        if ([theArray count] > 1)
        {
            self.theBackgroundView.hidden = NO;
            self.theCollectionView.scrollEnabled = YES;
        }

        gettingData = NO;
        
        [_theCollectionView reloadData];
    }
    
    if ([[notification name] isEqualToString:@"GetProfilePosts"])
    {
        NSDictionary *userInfo = notification.userInfo;
        profileArray = [userInfo objectForKey:@"myInfo"];
        [_theCollectionView reloadData];
    }
    
    if ([[notification name] isEqualToString:@"RefreshSearchVC"])
    {
        NSDictionary *userInfo = notification.userInfo;
        profileArray = [userInfo objectForKey:@"theSearchArray"];
        
        NSString *str = [[profileArray valueForKeyPath:@"following"] lastObject];

        NSLog(@"YYYYYY %@", profileArray);
        
        [_theCollectionView reloadData];
    }

}

#pragma mark - CollectionView Arguments

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (section == 0)
    {
        return 1;
    }
    
    if (section == 1)
    {
        
        if (theArray && theArray.count)
        {
            return theArray.count;
        }
        else
        {
            return 1;
        }

    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        
        SSUserProfileCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SSUserProfileCollectionCell" forIndexPath:indexPath];
    
        
        if ([userDefaults objectForKey:kTempUserID] != NULL)
        {
            if ([[userDefaults objectForKey:kMyID] isEqualToString:[userDefaults objectForKey:kTempUserID]])
            {
                cell.followButton.hidden = YES;
            }
            else
            {
                
                cell.followButton.hidden = NO;
            }
        }
        else
        {
            cell.followButton.hidden = YES;
        }
        
        [self configureProfileCell:cell atIndexPath:indexPath];
        return cell;
    }
    
    if (indexPath.section == 1)
    {
        SSUserProfileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SSUserProfileCollectionViewCell" forIndexPath:indexPath];
        [self configureCollectionViewCell:cell atIndexPath:indexPath];
        return cell;
    }
    
    return 0;

}

- (void)configureProfileCell:(SSUserProfileCollectionCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    cell.theBackgroundView.layer.masksToBounds = YES;
    cell.theBackgroundView.layer.cornerRadius = 38;
    
    cell.theImageView.layer.masksToBounds = YES;
    cell.theImageView.layer.cornerRadius = 36;
    
    if ([theArray count] > 0)
    {
        _theDetailDict = [theArray objectAtIndex:indexPath.row];
    }

    
    if ([ShortcutsWS isValidObject:[_theDetailDict objectForKey:@"picture"]])
    {
        NSURL *url = [[NSURL alloc] initWithString:[_theDetailDict objectForKey:@"picture"]];
        [cell.theImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"userplaceholder"]];
    }
    
    if ([userDefaults objectForKey:kFollowersNew] != NULL)
    {
         cell.followersLabel.text = [NSString stringWithFormat:@"%@ Followers", [userDefaults objectForKey:kFollowersNew]];
    }
    else
    {
        if ([ShortcutsWS isValidObject:[_theDetailDict objectForKey:@"followers"]])
        {
            cell.followersLabel.text = [NSString stringWithFormat:@"%@ Followers", [_theDetailDict objectForKey:@"followers"]];
            [userDefaults setObject:[_theDetailDict objectForKey:@"followers"] forKey:kFollowers];
            [userDefaults synchronize];
        }
    }
    
    if ([ShortcutsWS isValidObject:[_theDetailDict objectForKey:@"following"]])
    {
        cell.followingLabel.text = [NSString stringWithFormat:@"%@ Following", [_theDetailDict objectForKey:@"following"]];
        [userDefaults setObject:[_theDetailDict objectForKey:@"following"] forKey:kFollowing];
        [userDefaults synchronize];
        
    }

    
    if ([ShortcutsWS isValidObject:[_theDetailDict objectForKey:@"first"]] && [ShortcutsWS isValidObject:[_theDetailDict objectForKey:@"last"]])
    {
        cell.theNameLabel.text = [NSString stringWithFormat:@"%@ %@", [_theDetailDict objectForKey:@"first"], [_theDetailDict objectForKey:@"last"]];
        cell.theNameLabel.font = AppLightFontOfSize(14);
    }
    
   
    cell.followButton.layer.masksToBounds = YES;
    cell.followButton.layer.cornerRadius = 2;
    
    
    if ([[userDefaults objectForKey:kDidFriend] isEqualToString:@"1"])
    {
        [cell.followButton removeTarget:nil action:@selector(friendUser) forControlEvents:UIControlEventTouchUpInside];
        [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
        [cell.followButton addTarget:nil action:@selector(deleteFriend) forControlEvents:UIControlEventTouchUpInside];
        cell.followButton.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:61.0/255.0 blue:77.0/255.0 alpha:1];
    }
    else
    {
        
        [cell.followButton removeTarget:nil action:@selector(deleteFriend) forControlEvents:UIControlEventTouchUpInside];
        [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        [cell.followButton addTarget:nil action:@selector(friendUser) forControlEvents:UIControlEventTouchUpInside];
        cell.followButton.backgroundColor = [UIColor orangeColor];
    }
}

- (void)friendUser
{

    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kMyID], @"id", nil];
    NSMutableDictionary *params2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kTempUserID], @"id", nil];
    NSString *followersStr = [ShortcutsWS addition:[userDefaults objectForKey:kFollowers]];
    NSString *followingStr = [ShortcutsWS addition:[userDefaults objectForKey:kFollowing]];
    NSLog(@"ADD FFFollowers %@", followersStr);
    NSLog(@"ADD FFFollowing %@", followingStr);
    
    [userDefaults setObject:followersStr forKey:kFollowersNew];
    [userDefaults setObject:followingStr forKey:kFollowingNew];
    [userDefaults setObject:@"1" forKey:kDidFriend];
    [userDefaults synchronize];

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [APINetworkController updateFollowers:params2 followersNumber:followersStr];
        [APINetworkController updateFollowing:params followingNumber:followingStr];
        [APINetworkController addFriend:[userDefaults objectForKey:kMyID] friendID:[[theArray valueForKey:@"userID"] lastObject]];

        dispatch_async(dispatch_get_main_queue(), ^{
            if ([userDefaults objectForKey:kTempUserID] != NULL)
            {
                if (![[userDefaults objectForKey:kTempUserID] isEqualToString:[userDefaults objectForKey:kMyID]])
                {
                    gettingData = YES;
                    [APINetworkController getMyPosts:[userDefaults objectForKey:kTempUserID]];
                    [APINetworkController getMyFriend:[userDefaults objectForKey:kMyID] friendID:[userDefaults objectForKey:kTempUserID]];
                }
                else
                {
                    [APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
                }
                
            }
            else
            {
                [APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
            }

        });
        
    });


 }


- (void)deleteFriend
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kMyID], @"id", nil];
    NSMutableDictionary *params2 = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kTempUserID], @"id", nil];
    NSString *followersStr = [ShortcutsWS subtraction:[userDefaults objectForKey:kFollowers]];
    NSString *followingStr = [ShortcutsWS subtraction:[userDefaults objectForKey:kFollowing]];
    NSLog(@"SUB FFFollowers %@", followersStr);
    NSLog(@"SUB FFFollowing %@", followingStr);
    
    [userDefaults setObject:followersStr forKey:kFollowersNew];
    [userDefaults setObject:followingStr forKey:kFollowingNew];
    [userDefaults setObject:@"0" forKey:kDidFriend];
    [userDefaults synchronize];

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [APINetworkController updateFollowers:params2 followersNumber:followersStr];
        [APINetworkController updateFollowing:params followingNumber:followingStr];
        [APINetworkController deleteFriend:[userDefaults objectForKey:kMyID] friendID:[[theArray valueForKey:@"userID"] lastObject]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([userDefaults objectForKey:kTempUserID] != NULL)
            {
                if (![[userDefaults objectForKey:kTempUserID] isEqualToString:[userDefaults objectForKey:kMyID]])
                {
                    gettingData = YES;
                    [APINetworkController getMyPosts:[userDefaults objectForKey:kTempUserID]];
                    [APINetworkController getMyFriend:[userDefaults objectForKey:kMyID] friendID:[userDefaults objectForKey:kTempUserID]];
                }
                else
                {
                    [APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
                }
                
            }
            else
            {
                [APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
            }

        });
        
    });

}
- (void)configureCollectionViewCell:(SSUserProfileCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([theArray count] > 0)
    {
        _theDetailDict = [theArray objectAtIndex:indexPath.row];
    }
    
    if (![theArray count] > 0)
    {
        if (gettingData == NO)
        {
            cell.theMessageLabel.alpha = 0.0;
            cell.theNoTitleLabel.alpha = 0.0;
            
            [UIView animateWithDuration:1.0 animations:^{
                
                cell.theMessageLabel.alpha = 1.0;
                cell.theNoTitleLabel.alpha = 1.0;
                cell.theMessageLabel.text = @"This user currently has no posts.";
                cell.theNoTitleLabel.text = @"No Posts Yet";
                cell.theMessageLabel.font = AppLightFontOfSize(16);
                cell.theNoTitleLabel.font = AppLightFontOfSize(36);
                cell.noDataView.hidden = NO;
                
            }];
            
        }
        else
        {
            cell.theMessageLabel.alpha = 0.0;
            cell.theNoTitleLabel.alpha = 0.0;
            
            [UIView animateWithDuration:.75 animations:^{
                
                cell.theMessageLabel.alpha = 1.0;
                cell.theNoTitleLabel.alpha = 1.0;
                cell.theMessageLabel.text = @"Please wait.";
                cell.theNoTitleLabel.text = @"Loading Posts";
                cell.theMessageLabel.font = AppLightFontOfSize(16);
                cell.theNoTitleLabel.font = AppLightFontOfSize(36);
                cell.noDataView.hidden = NO;
                
            }];
        }
    }
    else
    {
        cell.noDataView.hidden = YES;
        
        NSDictionary *theDict = [theArray objectAtIndex:indexPath.row];
        
        
        if ([ShortcutsWS isValidObject:[theDict objectForKey:@"postURL"]])
        {
            
            if ([[theDict objectForKey:@"isImage"] isEqualToString:@"1"])
            {
                cell.moviePlayer.view.hidden = YES;
                
                cell.theImageView.alpha = 0.0;
                
                [UIView animateWithDuration:.75 animations:^{
                    
                    cell.theImageView.alpha = 1.0;
                    NSURL *url = [[NSURL alloc] initWithString:[theDict objectForKey:@"postURL"]];
                    [cell.theImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"]];;
                    
                }];
            }
            else if ([[theDict objectForKey:@"isImage"] isEqualToString:@"0"])
            {
                cell.moviePlayer.view.hidden = NO;
                
                cell.moviePlayer.controlStyle = MPMovieControlStyleNone;
                cell.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
                cell.moviePlayer.repeatMode = MPMovieRepeatModeOne;
                cell.moviePlayer.view.frame = CGRectMake(0, 0, 320, 424);
                [cell addSubview:cell.moviePlayer.view];
                NSString *filepath = [NSString stringWithFormat:@"%@", [theDict objectForKey:@"postURL"]];
                [cell.moviePlayer setContentURL:[NSURL URLWithString:filepath]];
                [cell.moviePlayer prepareToPlay];
                [cell.moviePlayer play];
                [cell.moviePlayer shouldAutoplay];
                NSString *tPostID = [theDict objectForKey:@"id"];
                [userDefaults setObject:tPostID forKey:@"tPostID"];
                [userDefaults synchronize];
            }
            
            
            
        }
        
        if ([ShortcutsWS isValidObject:[theDict objectForKey:@"theDate"]])
        {
            NSString *mysqlDatetime = [theDict objectForKey:@"theDate"];
            NSString *timeAgoFormattedDate = [NSDate mysqlDatetimeFormattedAsTimeAgo:mysqlDatetime];
            cell.theTimeLabel.text = timeAgoFormattedDate;
            cell.theTimeLabel.font = AppLightFontOfSize(9);
        }
        
    }
    
   }

#pragma mark - Collection View Arguments

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
         return CGSizeMake(320, 164);
    }
    else
    {
         return CGSizeMake(320, 424);
    }

}

@end
