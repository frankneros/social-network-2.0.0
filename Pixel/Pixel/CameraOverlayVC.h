//
//  CameraOverlayVC.h
//  Markt
//
//  Created by Eric Partyka on 5/30/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraOverlayVC : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

#pragma mark - Properties
@property (strong, nonatomic) UIImagePickerController *pickerReference;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *cameraButton;
@property (strong, nonatomic) IBOutlet UIImageView *theImageView;


@end
