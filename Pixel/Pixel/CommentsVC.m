//
//  CommentsVC.m
//  Markt
//
//  Created by Eric Partyka on 5/31/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "CommentsVC.h"
#import "AFNetworking.h"
#import "CommentsCell.h"
#import "Settings.h"
#import "APINetworkController.h"
#import "NSDate+NVTimeAgo.h"

static NSString * const CellIdentifier = @"commentsCell";

@interface CommentsVC ()

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UITableView *commentsTable;

@end

@implementation CommentsVC
{
    NSArray *comments;
    NSUserDefaults *userDefaults;
    RDRStickyKeyboardView *keyboardView;
    NSArray *postsData;
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self configureView];
    [self configureNavBar];
    [self getNotifications];
}

-(void)viewDidAppear:(BOOL)animated
{
    [APINetworkController getComments:self.commentID];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    userDefaults = [[NSUserDefaults alloc] init];
    comments = [[NSArray alloc] init];
    postsData = [[NSArray alloc] init];
    

}

#pragma mark - Private Methods

- (void)configureNavBar
{
    self.navigationItem.title = @"Comments";
}

- (void)configureView
{
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Comments";
    
    self.commentsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height) style:UITableViewStylePlain];
    self.commentsTable.dataSource = self;
    self.commentsTable.delegate = self;
    
    self.commentsTable.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    
    keyboardView = [[RDRStickyKeyboardView alloc] initWithScrollView:self.commentsTable];
    keyboardView.frame = self.view.bounds;
    keyboardView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [keyboardView.inputView.rightButton addTarget:self action:@selector(postANewComment) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:keyboardView];
}

- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshComments"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshCommentData"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshCommentsTable"
                                               object:nil];
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"RefreshComments"])
    {
        NSDictionary *userInfo = notification.userInfo;
        comments = [userInfo objectForKey:@"commentsArray"];
        NSLog(@"Comments Array %@", comments);
        [self.commentsTable reloadData];
    }
    
    if ([[notification name] isEqualToString:@"RefreshCommentData"])
    {
        [APINetworkController getComments:self.commentID];
        [self performSelector:@selector(updatePostComments) withObject:nil afterDelay:1];
    }
    
    if ([[notification name] isEqualToString:@"RefreshCommentsTable"])
    {
        [self.commentsTable reloadData];
    }
}


-(void)postANewComment
{
    [APINetworkController postComment:[userDefaults objectForKey:kMyID] postID:self.commentID comment:keyboardView.inputView.textView.text];
    keyboardView.inputView.textView.text = @"";
    
}

-(void)updatePostComments
{
    if ([comments count] > 0)
    {
        NSString *str = [ShortcutsWS addition:[[comments valueForKey:@"comments"] lastObject]];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.commentID, @"id", nil];
        [APINetworkController updatePostComments:params commentsNumber:str];
    
    }
    else
    {
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.commentID, @"id", nil];
        [APINetworkController updatePostComments:params commentsNumber:@"1"];
    }
    
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (comments && comments.count)
    {
        return comments.count;
    }
    else
    {
        return 0;
    }
}

#pragma mark - Table View Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifer = @"commentsCell";
    CommentsCell *cell = (CommentsCell *)[self.commentsTable dequeueReusableCellWithIdentifier:identifer];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCell" owner:self options:nil] objectAtIndex:0];
        cell.commentLabel.numberOfLines = 0;
        [cell.commentLabel sizeToFit];
        cell.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.commentLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    }
    
    NSDictionary *theDict = [comments objectAtIndex:indexPath.row];
    
    cell.commentLabel.text = [theDict objectForKey:@"comment"];
    
    NSString *firstStr = [NSString stringWithFormat:@"%@", [theDict objectForKey:@"first"]];
    NSString *lastStr = [NSString stringWithFormat:@"%@", [theDict objectForKey:@"last"]];
    NSString *combo = [NSString stringWithFormat:@"%@ %@", firstStr, lastStr];
    cell.nameLabel.text = [NSString stringWithFormat:@"@%@", [theDict objectForKey:@"username"]];
    
    NSString *mysqlDatetime = [theDict objectForKey:@"theDate"];
    NSString *timeAgoFormattedDate = [NSDate mysqlDatetimeFormattedAsTimeAgo:mysqlDatetime];
    cell.timeLabel.text = timeAgoFormattedDate;
    
    if ([theDict objectForKey:@"picture"] == nil || [[theDict objectForKey:@"picture"] isEqual:[NSNull null]]){
        cell.theImageView.image = [UIImage imageNamed:@"placerholdersquare.png"];
    } else {
        NSURL *url = [[NSURL alloc] initWithString:[theDict objectForKey:@"picture"]];
        [cell.theImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placerholdersquare.png"]];
    }
    
    cell.theImageView.layer.masksToBounds = YES;
    cell.theImageView.layer.cornerRadius = 20;
    
    return cell;
    
    

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *theDict = [comments objectAtIndex:indexPath.row];
    
    if ([theDict objectForKey:@"comment"] > 0 || [theDict objectForKey:@"comment"] != nil) {
        NSString *cellText = [theDict objectForKey:@"comment"];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:cellText];
        CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        return rect.size.height + 50;
    } else {
        return 0;
    }
    
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Actions

- (IBAction)backButton:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
