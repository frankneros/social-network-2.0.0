//
//  SSWalkthroughViewController.h
//  SocialSell
//
//  Created by Eric Partyka on 9/3/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"

@interface SSWalkthroughViewController : UIViewController <EAIntroDelegate>

#pragma mark - Properties

@property (nonatomic) BOOL didComeFromSettings;

@end
