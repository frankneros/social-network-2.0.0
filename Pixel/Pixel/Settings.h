//
//  Settings.h
//  Bite
//
//  Created by Rose on 11/4/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#ifndef Bite_Settings_h
#define Bite_Settings_h

#define kLoggedIn @"kLoggedIn"
#define kUserPicture @"kUserPicture"
#define kFirstName @"kFirstName"
#define kLastName @"kLastName"
#define kMyID @"kMyID"
#define kFriendID @"kFriendID"
#define kPostID @"kPostID"
#define kFirstTimeDone @"kFirstTimeDone"
#define kCommentID @"kCommentID"
#define kFollowing @"kFollowing"
#define kFollowers @"kFollowers"
#define kTempVideo @"kTempVideo"
#define kTempHashtags @"kTempHashtags"
#define kUsername @"kUsername"
#define kPassword @"kPassword"
#define kToken @"kToken"

#endif
