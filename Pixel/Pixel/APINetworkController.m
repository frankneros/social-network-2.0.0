//
//  APINetworkController.m
//  Pixel
//
//  Created by Eric Partyka on 7/11/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "APINetworkController.h"
#import "AFNetworking.h"
#import <CommonCrypto/CommonCrypto.h>
#import "AFPersioAPIClient.h"
#import "APIHelper.h"

@implementation APINetworkController

+(void)signUp:(NSString *)first last:(NSString *)last username:(NSString *)username password:(NSString *)password email:(NSString *)email imageData:(NSData *)imageData token:(NSString *)token
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"signUp.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[first dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"first"];
        [formData appendPartWithFormData:[last dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"last"];
        [formData appendPartWithFormData:[username dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"username"];
        [formData appendPartWithFormData:[password dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"password"];
        [formData appendPartWithFormData:[email dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"email"];
        [formData appendPartWithFileData:imageData name:@"userfile" fileName:@"img.jpg" mimeType:@"image/jpeg"];
        
        if (token == nil)
        {
            [formData appendPartWithFormData:[@"0" dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"token"];
        }
        else
        {
            [formData appendPartWithFormData:[token dataUsingEncoding:NSUTF8StringEncoding]
                                        name:token];
        }
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"GetSignUpData" object:nil userInfo:nil];
    

}

+(void)uploadUserToken:(NSString *)myID token:(NSString *)token
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"register_token.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[myID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        [formData appendPartWithFormData:[token dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"token"];
        
        NSLog(@"Token Added %@", token);
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];

}

+(void)updateInitalFriendsRelationship:(NSString *)myID friendID:(NSString *)friendID
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addFriend.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[myID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"myID"];
        [formData appendPartWithFormData:[friendID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"friendID"];
        
        NSLog(@"Inital Friends Updated");
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"InitalSignUp" object:nil userInfo:nil];

}

+(void)getLoginData:(NSString *)username password:(NSString *)password
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"userLogin.php?username=%@&password=%@", username, password]];
    NSURL *url = [NSURL URLWithString:strURL];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"Users"];
            NSLog(@"Login Data Array %@", theArray);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:theArray forKey:@"goodSignUp"];
            NSDictionary *userInfoLogin = [NSDictionary dictionaryWithObject:theArray forKey:@"goodSignUpFriends"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GoodSignUpFriends" object:nil userInfo:userInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GoodSignUp" object:nil userInfo:userInfoLogin];
            
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
        [[NSNotificationCenter defaultCenter] postNotificationName: @"BadSignUp" object:nil userInfo:nil];
        
    }];
    
    [operation start];
}

+(void)login:(NSString *)username password:(NSString *)password
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"userLogin.php?username=%@&password=%@", username, password]];
    NSURL *url = [NSURL URLWithString:strURL];
 
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"Users"];
            NSLog(@"Login Data Array %@", theArray);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:theArray forKey:@"signUpData"];
            NSDictionary *userInfoLogin = [NSDictionary dictionaryWithObject:theArray forKey:@"loginData"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateInitalFriends" object:nil userInfo:userInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"LoginToApp" object:nil userInfo:userInfoLogin];
            
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
        [[NSNotificationCenter defaultCenter] postNotificationName: @"LoginFailed" object:nil userInfo:nil];

    }];
    
    [operation start];
}

+(void)getUserList
{
    __block NSArray *theArray = nil;

    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, @"searchUsers.php"];
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Friends %@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:theArray forKey:@"allUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GetAllUsers" object:nil userInfo:userInfo];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)getAllFriends
{
    __block NSArray *theArray = nil;
    
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, @"getFriends.php"];
    NSURL *url = [NSURL URLWithString:strURL];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Friends %@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:theArray forKey:@"allUsers"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GetAllUsers" object:nil userInfo:userInfo];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];
}

+ (void)getMyFriend:(NSString *)myID friendID:(NSString *)friendID
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:myID, @"myID", friendID, @"friendID", nil];
    
    [[AFPersioAPIClient sharedClient] getPath:@"checkFriend.php" parameters:params
                                      success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         NSLog(@"Post Deleted");
         NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         
         NSLog(@"ZZZZZZZ %@", str);
         
         NSDictionary *userInfo = [NSDictionary dictionaryWithObject:responseObject forKey:@"CheckedFriend"];
         [[NSNotificationCenter defaultCenter] postNotificationName: @"CheckedFriend" object:nil userInfo:userInfo];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Did Not Check");
     }];
}

+(void)addFriend:(NSString *)myID friendID:(NSString *)friendID
{
    
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addFriend.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[myID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"myID"];
        [formData appendPartWithFormData:[friendID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"friendID"];
        
        NSLog(@"Friend Added");
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateFollowing" object:nil userInfo:nil];
    
}

+(void)updateFollowing:(NSMutableDictionary *)params followingNumber:(NSString *)followingNumber
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updateFollowing.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[followingNumber dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"following"];
        
        NSLog(@"Following Updated");
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateFollowers" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateFollowersDelete" object:nil userInfo:nil];

}

+(void)updateFollowers:(NSMutableDictionary *)params followersNumber:(NSString *)followersNumber
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updateFollowers.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[followersNumber dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"followers"];
        
        NSLog(@"Followers Updated");
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshFollow" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshFollow" object:nil userInfo:nil];
    
    
}

+(void)refreshFollow:(NSString *)myID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getUserSettings.php?id=%@", myID]];
    NSURL *url = [NSURL URLWithString:strURL];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    __block NSArray *theFriendArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            theFriendArray = [theArray valueForKey:@"friendID"];
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:theArray forKey:@"refreshFollowArray"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshSearchArray" object:nil userInfo:userInfo];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)getUserSettings:(NSString *)myID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getUserSettings.php?id=%@", myID]];
    NSURL *url = [NSURL URLWithString:strURL];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    __block NSArray *theFriendArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"User Settings %@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            theFriendArray = [theArray valueForKey:@"friendID"];
            NSLog(@"Friends %@", theFriendArray);
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"theSearchArray", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshSearchVC" object:nil userInfo:userInfo];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)getPosts:(NSString *)myID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getPosts.php?myID=%@", myID]];
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"postsArray", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshPosts" object:nil userInfo:userInfo];
    
        }
        
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)getLikes:(NSString *)myID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getLikes.php?userID=%@", myID]];
    NSURL *url = [NSURL URLWithString:strURL];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"likesArray", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshLikes" object:nil userInfo:userInfo];
            NSLog(@"Likes Array %@", theArray);
            
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)likePostAfterCountOne:(NSMutableDictionary *)params likeNumber:(NSString *)likeNumber
{
    NSString *str = [APIHelper baseURL];
    
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updatePostLikes.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[likeNumber dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"likes"];
        
        NSLog(@"Like Added");
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];

}

+(void)reportPost:(NSString *)userID postID:(NSString *)postID
{
    NSString *str = [APIHelper baseURL];
    
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"reportPost.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData)
    {
        
        [formData appendPartWithFormData:[userID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        [formData appendPartWithFormData:[postID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"postID"];
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
}


+(void)addLikesToPostsTable:(NSString *)myID postID:(NSString *)postID
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addLikes.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[myID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        
        [formData appendPartWithFormData:[postID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"likedPostID"];
        
        NSLog(@"Likes Table Updated");
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
}

+(void)addInitialLikes:(NSMutableDictionary *)params likeNumber:(NSString *)likeNumber
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updatePostLikes.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[likeNumber dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"likes"];
        
        NSLog(@"Like Added");
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
}

+(void)deletePost:(NSMutableDictionary *)params
{
    [[AFPersioAPIClient sharedClient] getPath:@"deletePost.php" parameters:params
                                      success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         NSLog(@"Post Deleted");
         [[NSNotificationCenter defaultCenter] postNotificationName: @"ReloadPostsTable" object:nil userInfo:nil];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Post Did Not Delete");
     }];

}

+(void)uploadVideo:(NSData *)videoData myID:(NSString *)myID description:(NSString *)description isVideo:(NSString *)isVideo isImage:(NSString *)isImage
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addPost.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[myID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        
        [formData appendPartWithFileData:videoData name:@"userfile" fileName:@"video.mov" mimeType:@"video/quicktime"];
        [formData appendPartWithFormData:[description dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"description"];
        [formData appendPartWithFormData:[isVideo dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"isVideo"];
        [formData appendPartWithFormData:[isImage dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"isImage"];
        
        
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdatePosts" object:nil userInfo:nil];

}

+(void)uploadImage:(NSData *)imageData myID:(NSString *)myID description:(NSString *)description isVideo:(NSString *)isVideo isImage:(NSString *)isImage
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addPost.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[myID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        [formData appendPartWithFileData:imageData name:@"userfile" fileName:@"img.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFormData:[description dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"description"];
        [formData appendPartWithFormData:[isVideo dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"isVideo"];
        [formData appendPartWithFormData:[isImage dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"isImage"];
        
        
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdatePosts" object:nil userInfo:nil];

}

+(void)uploadAndUpdatePosts:(NSMutableDictionary *)params postNumber:(NSString *)postNumber
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updatePosts.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[postNumber dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"posts"];
        
        NSLog(@"Number Posts %@", postNumber);
        
        
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];

}

+(void)getComments:(NSString *)postID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getComments.php?postID=%@", postID]];
    NSURL *url = [NSURL URLWithString:strURL];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Comments %@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"commentsArray", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshComments" object:nil userInfo:userInfo];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)postComment:(NSString *)userID postID:(NSString *)postID comment:(NSString *)comment
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addComment.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[userID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        [formData appendPartWithFormData:[postID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"postID"];
        [formData appendPartWithFormData:[comment dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"comment"];
        
        NSLog(@"Comment Added");
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshCommentData" object:nil userInfo:nil];

}

+(void)updatePostComments:(NSMutableDictionary *)params commentsNumber:(NSString *)commentsNumber
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updatePostComments.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[commentsNumber dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"comments"];
        
        NSLog(@"Comment Added");
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshCommentsTable" object:nil userInfo:nil];

}

+(void)getMyPosts:(NSString *)userID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getMyPosts.php?userID=%@", userID]];
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"myPosts", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshProfile" object:nil userInfo:userInfo];
            
            NSDictionary *newDict = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"newPosts", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GotNewPosts" object:nil userInfo:newDict];

        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];

}

+(void)updateProfilePicture:(NSMutableDictionary *)params imageData:(NSData *)imageData
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"updatePicture.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"userfile" fileName:@"img.jpg" mimeType:@"image/jpeg"];
        
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"GetProfilePostsAgain" object:nil userInfo:nil];
}

+(void)getMyProfile:(NSString *)myID
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, [NSString stringWithFormat:@"getMyProfile.php?id=%@", myID]];
    NSURL *url = [NSURL URLWithString:strURL];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *userProfileDict = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"myInfo", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GetProfilePosts" object:nil userInfo:userProfileDict];
            
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];
}

+(void)uploadHashtags:(NSString *)userID postID:(NSString *)postID hashtag:(NSString *)hashtag
{
    NSString *str = [APIHelper baseURL];
    NSURL *url = [NSURL URLWithString:str];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"addHashtags.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFormData:[userID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"userID"];
        [formData appendPartWithFormData:[postID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"postID"];
        [formData appendPartWithFormData:[hashtag dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"hashtag"];



    
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"FinishUpload" object:nil userInfo:nil];


}

+(void)getHashtags
{
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, @"getHashtags.php"];
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    __block NSArray *theArray = nil;
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@", JSON);
        
        if ([JSON count] > 0) {
            theArray = [JSON objectForKey:@"posts"];
            NSDictionary *theDict = [NSDictionary dictionaryWithObjectsAndKeys:theArray, @"hashtags", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"GotHashtags" object:nil userInfo:theDict];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    
    [operation start];
 
}

+(void)deleteFriend:(NSString *)myID friendID:(NSString *)friendID
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:myID, @"myID", friendID, @"friendID", nil];
    
    [[AFPersioAPIClient sharedClient] getPath:@"deleteFriend.php" parameters:params
                                      success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         NSLog(@"Friend Deleted");
         [[NSNotificationCenter defaultCenter] postNotificationName: @"UpdateFollowingDelete" object:nil userInfo:nil];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Post Did Not Delete");
     }];

}
@end
