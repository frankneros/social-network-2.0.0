//
//  HeaderView.h
//  Markt
//
//  Created by Eric Partyka on 5/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIButton *optionsButton;
@property (strong, nonatomic) IBOutlet UIImageView *userProfilePicture;
@property (strong, nonatomic) IBOutlet UILabel *usersName;
@property (strong, nonatomic) IBOutlet UILabel *theDate;
@property (strong, nonatomic) IBOutlet UIButton *theUsername;
@property (strong, nonatomic) IBOutlet UIButton *theImageButton;

@end
