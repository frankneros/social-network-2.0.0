//
//  AdddPicture.m
//  Markt
//
//  Created by Eric Partyka on 5/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "AdddPicture.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "CameraOverlayVC.h"
#import "CameraViewContainer.h"
#import "MainTabBar.h"

@interface AdddPicture () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, YCameraViewControllerDelegate>

#pragma mark - Properties
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) NSArray *results;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *uploadButton;

@end

@implementation AdddPicture
{
    CameraOverlayVC *overlay;
    NSUserDefaults *userDefaults;
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View Lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    if ([[userDefaults objectForKey:@"taken"] isEqualToString:@"Yes"]) {
        YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
        camController.delegate=self;
        [self dismissViewControllerAnimated:NO completion:^{
            [self presentViewController:camController animated:NO completion:nil];
        }];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNavBar];
    
    YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
    camController.delegate=self;
    [self presentViewController:camController animated:NO completion:nil];
    
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    self.userDefaults = [[NSUserDefaults alloc] init];
    
    [self.uploadButton setEnabled:NO];
    
    self.results = [[NSArray alloc] init];

    // Do any additional setup after loading the view.
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavBar
{
    self.navigationItem.title = @"Add Picture";
}

#pragma mark - YCameraViewController Delegate

- (void)didFinishPickingImage:(UIImage *)image
{

    UIImage *theImage = image;
    NSData *imageData = UIImageJPEGRepresentation(theImage, 100);
    
    [self.userDefaults setObject:imageData forKey:@"tempImg"];
    [self.userDefaults synchronize];
}

-(void)yCameraControllerDidCancel
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    mainView.selectedIndex = 0;
    [self presentViewController:mainView animated:NO completion:nil];

}

- (void)yCameraControllerdidSkipped
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    mainView.selectedIndex = 0;
    [self presentViewController:mainView animated:NO completion:nil];

}

#pragma mark - Actions

- (IBAction)newPicture:(id)sender
{
    YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
    camController.delegate=self;
    [self presentViewController:camController animated:NO completion:nil];
}

@end
