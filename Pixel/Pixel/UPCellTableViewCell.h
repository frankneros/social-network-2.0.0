//
//  UPCellTableViewCell.h
//  Markt
//
//  Created by Eric Partyka on 6/1/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UPCellTableViewCell : UITableViewCell

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UITextField *theTextFiels;

@end
