//
//  ShortcutsWS.h
//  Pixel
//
//  Created by Eric Partyka on 9/26/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShortcutsWS : NSObject

#pragma mark - Instance Methods
+ (NSString *)getCurrentDate;

+ (NSMutableString *)secureTheString:(NSString *)stringToEncode;

+ (NSString *)addition:(NSString *)parameter;

+ (NSString *)subtraction:(NSString *)parameter;

+ (NSData *)resizeImage:(NSData *)parameter;

+ (BOOL)isValidObject:(NSString *)parameter;

+ (BOOL)isEmailValid:(NSString *)email;

@end
