//
//  SSUserProfileCollectionViewCell.m
//  SocialSell
//
//  Created by Eric Partyka on 8/2/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SSUserProfileCollectionViewCell.h"

@implementation SSUserProfileCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"StopMoviePlayer"])
    {
        NSLog (@"Successfully received the test notification!");
        [self.moviePlayer stop];
        
    }
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    self.moviePlayer = [[MPMoviePlayerController alloc] init];
    [self addSubview:self.moviePlayer.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"ProfileMoviePlayer"
                                               object:nil];
}


@end
