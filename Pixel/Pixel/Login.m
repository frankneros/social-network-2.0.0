//
//  Login.m
//  Bite
//
//  Created by Eric Partyka on 10/11/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import "Login.h"
#import "AFNetworking.h"
#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "AFPersioAPIClient.h"
#import "Settings.h"
#import "MBProgressHUD.h"
#import "SignUp.h"
#include <CommonCrypto/CommonDigest.h>
#import "MainTabBar.h"
#import "APINetworkController.h"
#import "SettingsDefines.h"
#import "MBProgressHUD.h"

#define kSalt @"rS24PoJSARl7jCV4ICoimaAeK6fzfoN1"

@interface Login ()

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UITextField *theUsername;
@property (strong, nonatomic) IBOutlet UITextField *thePassword;
@property (strong, nonatomic) IBOutlet UIButton *loginText;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;

@end

@implementation Login
{
    NSUserDefaults *userDefaults;
    NSArray *theArray;
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View Lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    
    [self getNotifications];
    [self configureView];
    
   
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[userDefaults objectForKey:@"USERTOKEN"] isEqualToString:@"TOKENSAVED"]) {
        NSLog(@"Do Nothing");
    } else {
        NSString *str = @"1";
        [userDefaults setObject:str forKey:@"USERTOKEN"];
        [userDefaults synchronize];
    }
    
   
    
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureView
{
    self.theUsername.layer.masksToBounds = YES;
    self.theUsername.layer.cornerRadius = 2;
    self.thePassword.layer.masksToBounds = YES;
    self.thePassword.layer.cornerRadius = 2;
    self.loginText.layer.masksToBounds = YES;
    self.loginText.layer.cornerRadius = 2;
    self.signUpButton.layer.masksToBounds = YES;
    self.signUpButton.layer.cornerRadius = 2;
}

- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"LoginToApp"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"LoginFailed"
                                               object:nil];
  
}


- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"LoginToApp"])
    {
        NSLog (@"Successfully received the test notification!");
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        NSDictionary *userInfo = notification.userInfo;
        theArray = [userInfo objectForKey:@"loginData"];
        
        for (NSDictionary *theDict in theArray) {
            NSString *myID = [theDict objectForKey:@"id"];
            [userDefaults setObject:myID forKey:kMyID];
            [userDefaults setObject:@"1" forKey:kLoggedIn];
            [userDefaults synchronize];
        }
        
        UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
        mainView.selectedIndex = 0;
        [self presentViewController:mainView animated:NO completion:nil];

    }
    
    if ([[notification name] isEqualToString:@"LoginFailed"])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [userDefaults setObject:@"0" forKey:kLoggedIn];
        [userDefaults synchronize];
        
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Whoops" message:@"Login Failed. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Actions

- (IBAction)loginButton:(id)sender
{
    
    if ([self.theUsername.text isEqualToString:@""] || [self.thePassword.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please fill in all of the fields!"  delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        return;
    }
    else
    {
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Loggin In";
        progressHUD.mode = MBProgressHUDAnimationFade;
        
        NSString *saltedPassword = [NSString stringWithFormat:@"%@%@", self.thePassword.text, kSalt];
        NSMutableString* output = [ShortcutsWS secureTheString:saltedPassword];
        [APINetworkController login:self.theUsername.text password:output];

    }
}
    
- (IBAction)hideKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)goToSignUp:(id)sender
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignUp *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"signUp"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:detailVC];
    [self presentViewController:navigationController animated:YES completion:nil];
    
}
@end
