//
//  UPCellTableViewCell.m
//  Markt
//
//  Created by Eric Partyka on 6/1/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "UPCellTableViewCell.h"

@implementation UPCellTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
