//
//  BiteCell.h
//  Bite
//
//  Created by Eric Partyka on 10/4/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureCell: UITableViewCell

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIImageView *postedPicture;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;

@end
