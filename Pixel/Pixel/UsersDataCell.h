//
//  UsersDataCell.h
//  Markt
//
//  Created by Eric Partyka on 5/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

@interface UsersDataCell : UICollectionViewCell

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIImageView *theImageView;
@property (strong, nonatomic)  MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UIView *theView;

@end
