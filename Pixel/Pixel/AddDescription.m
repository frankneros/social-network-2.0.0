//
//  AddDescription.m
//  Markt
//
//  Created by Eric Partyka on 6/1/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "AddDescription.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "MainTabBar.h"
#import "APINetworkController.h"
#import "MBProgressHUD.h"

@interface AddDescription () <UITextViewDelegate, AMAttributedHighlightLabelDelegate>

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UITextView *theDescription;

@end

@implementation AddDescription
{
    NSUserDefaults *userDefaults;
    NSArray *results;
    NSString *theStr;
    UILabel *theLabel;
    NSMutableArray *hashtagsArray;
    
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNavBar];
    [self getNotifications];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    self.theDescription.delegate = self;
    hashtagsArray = [[NSMutableArray alloc] init];

    results = [[NSArray alloc] init];
    
    [APINetworkController getUserSettings:[userDefaults objectForKey:kMyID]];
    
    NSData *imageData = [userDefaults dataForKey:@"tempImg"];
    UIImage *theImage = [UIImage imageWithData:imageData];
    
    // Do any additional setup after loading the view.
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)configureNavBar
{
    self.navigationItem.title = @"Add Description";
}

- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"UpdatePosts"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshSearchVC"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GotNewPosts"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"FinishUpload"
                                               object:nil];

}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"UpdatePosts"])
    {
        NSLog (@"Successfully received the test notification!");
        if ([results count] > 0)
        {
            
            NSString *str = [ShortcutsWS addition:[[results valueForKeyPath:@"posts"] lastObject]];
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kMyID], @"id", nil];
            [APINetworkController uploadAndUpdatePosts:params postNumber:str];
            [self performSelector:@selector(getMyPosts) withObject:nil afterDelay:1];
            
            
        }
        else
        {
            NSString *str = [ShortcutsWS addition:@"1"];
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kMyID], @"id", nil];
            [APINetworkController uploadAndUpdatePosts:params postNumber:str];
            [self performSelector:@selector(getMyPosts) withObject:nil afterDelay:1];
        }
        
    }
    
    if ([[notification name] isEqualToString:@"GotNewPosts"])
    {
        NSDictionary *userInfo = notification.userInfo;
        NSArray *myArray = [[NSArray alloc] init];
        myArray = [userInfo objectForKey:@"newPosts"];
        NSMutableArray *last = [[NSMutableArray alloc] init];
        [last addObject:[myArray lastObject]];
        for (NSDictionary *theDict in last) {
            NSString *postID = [theDict objectForKey:@"id"];
            
            NSString * aString = [theDict objectForKey:@"description"];
            NSMutableArray *substrings = [NSMutableArray new];
            NSScanner *scanner = [NSScanner scannerWithString:aString];
            [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before #
            while(![scanner isAtEnd]) {
                NSString *substring = nil;
                [scanner scanString:@"#" intoString:nil]; // Scan the # character
                if([scanner scanUpToString:@" " intoString:&substring]) {
                    // If the space immediately followed the #, this will be skipped
                    [substrings addObject:substring];
                    NSLog(@"The Hashtag %@", substring);
                    [APINetworkController uploadHashtags:[userDefaults objectForKey:kMyID] postID:postID hashtag:substring];
                }
                
                [scanner scanUpToString:@"#" intoString:nil]; // Scan all characters before next #
            }
            
        }
     
    }
    
    if ([[notification name] isEqualToString:@"FinishUpload"])
    {
        [self goBackToTheMainFeed];
    }
    
    if ([[notification name] isEqualToString:@"RefreshSearchVC"])
    {
        NSDictionary *userInfo = notification.userInfo;
        results = [userInfo objectForKey:@"theSearchArray"];
    }

}

-(void)getMyPosts
{
    //[APINetworkController getMyPosts:[userDefaults objectForKey:kMyID]];
    [self goBackToTheMainFeed];
}

-(void)goBackToTheMainFeed
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    mainView.selectedIndex = 0;
    [self presentViewController:mainView animated:NO completion:nil];
}

#pragma mark - Actions

- (IBAction)upload:(id)sender
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progressHUD.labelText = @"Posting";
    progressHUD.mode = MBProgressHUDAnimationFade;
    
    [self.view endEditing:YES];
    
    if ([userDefaults objectForKey:kTempVideo] != NULL)
    {

        NSData *videoData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:[userDefaults objectForKey:kTempVideo]]];
        [APINetworkController uploadVideo:videoData myID:[userDefaults objectForKey:kMyID] description:self.theDescription.text isVideo:@"1" isImage:@"0"];

    }
    else
    {
        
        if ([userDefaults objectForKey:@"newImg"] == NULL)
        {
            NSData *imgData = [ShortcutsWS resizeImage:[userDefaults dataForKey:@"tempImg"]];
            [APINetworkController uploadImage:imgData myID:[userDefaults objectForKey:kMyID] description:self.theDescription.text isVideo:@"0" isImage:@"1"];

        }
        else
        {
            NSData *imgData = [ShortcutsWS resizeImage:[userDefaults dataForKey:@"newImg"]];
            [APINetworkController uploadImage:imgData myID:[userDefaults objectForKey:kMyID] description:self.theDescription.text isVideo:@"0" isImage:@"1"];
        }
    }
}



- (IBAction)cancel:(id)sender
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    mainView.selectedIndex = 0;
    [self presentViewController:mainView animated:NO completion:nil];
}
@end
