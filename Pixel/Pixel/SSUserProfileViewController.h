//
//  SSUserProfileViewController.h
//  SocialSell
//
//  Created by Eric Partyka on 8/2/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSUserProfileViewController : UIViewController

#pragma mark - Properties
@property (strong, nonatomic) NSDictionary *theDetailDict;
@property (nonatomic) BOOL cameFromMenu;
@property (nonatomic) BOOL didAddFriend;
@property (nonatomic) BOOL cameFromSearch;
@property (strong, nonatomic) NSString *userIDStr;
@property (readwrite, nonatomic) int userInt;
@end
