//
//  CameraViewContainer.h
//  Markt
//
//  Created by Eric Partyka on 5/30/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AviarySDK/AviarySDK.h>

@interface CameraViewContainer : UIViewController <AFPhotoEditorControllerDelegate>

@end
