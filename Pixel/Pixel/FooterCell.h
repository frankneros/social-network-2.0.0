//
//  FooterCell.h
//  Markt
//
//  Created by Eric Partyka on 5/31/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterCell : UITableViewCell

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIButton *leaveComment;
@property (strong, nonatomic) IBOutlet UIButton *favoritePost;
@property (strong, nonatomic) IBOutlet UILabel *numberofComments;
@property (strong, nonatomic) IBOutlet UILabel *numberofLikes;

@end
