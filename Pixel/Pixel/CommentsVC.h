//
//  CommentsVC.h
//  Markt
//
//  Created by Eric Partyka on 5/31/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDRStickyKeyboardView.h"

@interface CommentsVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

#pragma mark - Properties
@property (strong, nonatomic) NSString *commentID;

@end
