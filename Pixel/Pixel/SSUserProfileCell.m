//
//  SSUserProfileCell.m
//  SocialSell
//
//  Created by Eric Partyka on 8/2/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SSUserProfileCell.h"

@implementation SSUserProfileCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


@end
