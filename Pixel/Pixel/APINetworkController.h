//
//  APINetworkController.h
//  Pixel
//
//  Created by Eric Partyka on 7/11/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APINetworkController : NSObject

+(void)reportPost:(NSString *)userID postID:(NSString *)postID;

+(void)signUp:(NSString *)first last:(NSString *)last username:(NSString *)username password:(NSString *)password email:(NSString *)email imageData:(NSData *)imageData token:(NSString *)token;

+(void)uploadUserToken:(NSString *)myID token:(NSString *)token;

+(void)updateInitalFriendsRelationship:(NSString *)myID friendID:(NSString *)friendID;

+(void)login:(NSString *)username password:(NSString *)password;

+(void)getLoginData:(NSString *)username password:(NSString *)password;

+(void)getUserList;

+(void)addFriend:(NSString *)myID friendID:(NSString *)friendID;

+(void)updateFollowing:(NSMutableDictionary *)params followingNumber:(NSString *)followingNumber;

+(void)updateFollowers:(NSMutableDictionary*)params followersNumber:(NSString *)followersNumber;

+(void)refreshFollow:(NSString *)myID;

+(void)getUserSettings:(NSString *)myID;

+(void)getPosts:(NSString *)myID;

+(void)getLikes:(NSString *)myID;

+(void)likePostAfterCountOne:(NSMutableDictionary *)params likeNumber:(NSString *)likeNumber;

+(void)addLikesToPostsTable:(NSString *)myID postID:(NSString *)postID;

+(void)addInitialLikes:(NSMutableDictionary *)params likeNumber:(NSString *)likeNumber;

+(void)deletePost:(NSMutableDictionary *)params;

+(void)uploadVideo:(NSData *)videoData myID:(NSString *)myID description:(NSString *)description isVideo:(NSString *)isVideo isImage:(NSString *)isImage;

+(void)uploadImage:(NSData *)imageData myID:(NSString *)myID description:(NSString *)description isVideo:(NSString *)isVideo isImage:(NSString *)isImage;

+(void)uploadAndUpdatePosts:(NSMutableDictionary *)params postNumber:(NSString *)postNumber;

+(void)getComments:(NSString *)postID;

+(void)postComment:(NSString *)userID postID:(NSString *)postID comment:(NSString *)comment;

+(void)updatePostComments:(NSMutableDictionary *)params commentsNumber:(NSString *)commentsNumber;

+(void)getMyPosts:(NSString *)userID;

+(void)updateProfilePicture:(NSMutableDictionary *)params imageData:(NSData *)imageData;

+(void)getMyProfile:(NSString *)myID;

+(void)uploadHashtags:(NSString *)userID postID:(NSString *)postID hashtag:(NSString *)hashtag;

+(void)getHashtags;

+(void)deleteFriend:(NSString *)myID friendID:(NSString *)friendID;

+(void)getAllFriends;

+(void)getMyFriend:(NSString *)myID friendID:(NSString *)friendID;


@end
