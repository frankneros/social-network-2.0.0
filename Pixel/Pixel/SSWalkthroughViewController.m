//
//  SSWalkthroughViewController.m
//  SocialSell
//
//  Created by Eric Partyka on 9/3/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SSWalkthroughViewController.h"
#import "SMPageControl.h"
#import <QuartzCore/QuartzCore.h>
#import "Login.h"
#import "SettingsDefines.h"

static NSString * const sampleDescription1 = @"Description here.";
static NSString * const sampleDescription2 = @"Description here.";
static NSString * const sampleDescription3 = @"Description here.";
static NSString * const sampleDescription4 = @"Description here. ";

@interface SSWalkthroughViewController ()

@end

@implementation SSWalkthroughViewController
{
    UIView *rootView;
    NSUserDefaults *userDefaults;
}


#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
     [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = NO;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [self showCustomIntro];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    rootView = self.view;
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    //[self showCustomIntro];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)showCustomIntro {
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"Welcome";
    [page1 setTitleFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [page1 setDescFont:[UIFont fontWithName:@"Helvetica-Light" size:15]];
    page1.titlePositionY = 240;
    page1.desc = sampleDescription1;
    page1.descPositionY = 210;
    page1.bgImage = [UIImage imageNamed:@"theBack"];
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iPhone"]];
    page1.titleIconPositionY = 100;
    page1.showTitleView = NO;
    
    EAIntroPage *page2 = [EAIntroPage page];
    [page2 setTitleFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [page2 setDescFont:[UIFont fontWithName:@"Helvetica-Light" size:15]];
    page2.title = @"Title Here";
    page2.titlePositionY = 240;
    page2.desc = sampleDescription2;
    page2.descPositionY = 210;
    page2.bgImage = [UIImage imageNamed:@"theBack"];
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"post"]];
    page2.titleIconPositionY = 100;
    
    EAIntroPage *page3 = [EAIntroPage page];
    [page3 setTitleFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [page3 setDescFont:[UIFont fontWithName:@"Helvetica-Light" size:15]];
    page3.title = @"Title Here";
    page3.titlePositionY = 240;
    page3.desc = sampleDescription3;
    page3.descPositionY = 210;
    page3.bgImage = [UIImage imageNamed:@"theBack"];
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"thesearch"]];
    page3.titleIconPositionY = 100;
    
    EAIntroPage *page4 = [EAIntroPage page];
    [page4 setTitleFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [page4 setDescFont:[UIFont fontWithName:@"Helvetica-Light" size:15]];
    page4.title = @"Title Here";
    page4.titlePositionY = 240;
    page4.desc = sampleDescription4;
    page4.descPositionY = 210;
    page4.bgImage = [UIImage imageNamed:@"theBack"];
    page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail"]];
    page4.titleIconPositionY = 100;
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:rootView.bounds andPages:@[page1,page2,page3,page4]];
    intro.titleViewY = 120;
    intro.tapToNext = YES;
    [intro setDelegate:self];
    
    SMPageControl *pageControl = [[SMPageControl alloc] init];
    pageControl.pageIndicatorImage = [UIImage imageNamed:@"pageDot"];
    pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"selectedPageDot"];
    [pageControl sizeToFit];
    intro.pageControl = (UIPageControl *)pageControl;
    intro.pageControlY = 130.0f;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setFrame:CGRectMake((320-270)/2, [UIScreen mainScreen].bounds.size.height - 80, 270, 50)];
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitle:@"SKIP INTRO" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 4;
    intro.skipButton = btn;
    
    [intro showInView:rootView animateDuration:0.3];
}

- (void)introDidFinish:(EAIntroView *)introView
{
    if (self.didComeFromSettings == YES)
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        
        [userDefaults setObject:@"1" forKey:kWalkthroughCompleted];
        [userDefaults synchronize];

        
        UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        Login *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"Login"];
        [self presentViewController:mainView animated:NO completion:nil];
    }
}
@end
