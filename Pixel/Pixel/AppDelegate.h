//
//  AppDelegate.h
//  Bite
//
//  Created by Eric Partyka on 10/2/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) UIWindow *window;

@end
