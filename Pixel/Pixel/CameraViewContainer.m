//
//  CameraViewContainer.m
//  Markt
//
//  Created by Eric Partyka on 5/30/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "CameraViewContainer.h"
#import "CameraOverlayVC.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "MainTabBar.h"
#import "AddDescription.h"

static NSString * const kAFAviaryAPIKey = @"ab235be6ea48e875";
static NSString * const kAFAviarySecret = @"438ac469a5e81dde";

@interface CameraViewContainer ()

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIImageView *theImageView;

@end

@implementation CameraViewContainer
{
    NSUserDefaults *userDefaults;
    NSString *apiKey;
    NSString *apiSecret;
    BOOL didEditPhoto;
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
     // Do any additional setup after loading the view.
    
    didEditPhoto = NO;
    
    [self configureNavBar];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *imageData = [userDefaults dataForKey:@"tempImg"];
    UIImage *theImage = [UIImage imageWithData:imageData];
    self.theImageView.image = theImage;
    
    apiKey = @"1151bc938826012a";
    apiSecret = @"07bcc1d0e7ee137c";
   
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavBar
{
   self.navigationItem.title = @"Edit";
}

#pragma mark - Actions

- (IBAction)filters:(id)sender
{

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [AFPhotoEditorController setAPIKey:kAFAviaryAPIKey secret:kAFAviarySecret];
    });
    
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:self.theImageView.image];
    [editorController setDelegate:self];
    [self presentViewController:editorController animated:YES completion:nil];
}

- (IBAction)cancel:(id)sender
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    mainView.selectedIndex = 0;
    [self presentViewController:mainView animated:NO completion:nil];
    
}

- (IBAction)next:(id)sender
{
    if (didEditPhoto == NO)
    {
        [userDefaults setObject:NULL forKey:@"newImg"];
        [userDefaults synchronize];
    }
    
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddDescription *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"descriptionVC"];
    [userDefaults setObject:NULL forKey:kTempVideo];
    [userDefaults synchronize];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mainView];
    [self presentViewController:nav animated:NO completion:nil];
}

#pragma mark - Aviary Delegate

- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image
{
    didEditPhoto = YES;
    
    self.theImageView.image = image;
    NSData *imageData = UIImageJPEGRepresentation(image, 100);
    
    [userDefaults setObject:imageData forKey:@"newImg"];
    [userDefaults synchronize];

    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
{
    // Handle cancellation here
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
