//
//  SSUserProfileCollectionViewCell.h
//  SocialSell
//
//  Created by Eric Partyka on 8/2/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

@interface SSUserProfileCollectionViewCell : UICollectionViewCell

#pragma mark - Properties
@property (strong, nonatomic)  MPMoviePlayerController *moviePlayer;
@property (weak, nonatomic) IBOutlet UIImageView *theImageView;
@property (weak, nonatomic) IBOutlet UIView *theStatusView;
@property (weak, nonatomic) IBOutlet UILabel *theStatusLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *theTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *theTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *theNoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *theMessageLabel;
@property (strong, nonatomic) IBOutlet UIView *noDataView;

@end
