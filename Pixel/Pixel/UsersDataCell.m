//
//  UsersDataCell.m
//  Markt
//
//  Created by Eric Partyka on 5/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "UsersDataCell.h"

@implementation UsersDataCell
@synthesize theImageView;
@synthesize moviePlayer;
@synthesize theView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
