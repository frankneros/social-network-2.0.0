//
//  BiteCell.m
//  Bite
//
//  Created by Eric Partyka on 10/4/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import "PictureCell.h"

@implementation PictureCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
