//
//  SSPostsFooterCell.h
//  SocialSell
//
//  Created by Eric Partyka on 7/20/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSPostsFooterCell : UITableViewCell

#pragma mark - Properties
@property (weak, nonatomic) IBOutlet UIButton *optionsButton;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *commentsButton;

@end
