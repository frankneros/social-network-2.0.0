//
//  UserProfileCell.h
//  Bite
//
//  Created by Eric Partyka on 5/19/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileCell : UITableViewCell

#pragma mark - Properties

@property (strong, nonatomic) IBOutlet UIImageView *theImageView;
@property (strong, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIButton *uploadProfilePicture;
@property (strong, nonatomic) IBOutlet UILabel *posts;
@property (strong, nonatomic) IBOutlet UIView *followers;
@property (strong, nonatomic) IBOutlet UILabel *following;
@property (strong, nonatomic) IBOutlet UILabel *followersNumber;
@property (strong, nonatomic) IBOutlet UILabel *followingNumber;
@property (strong, nonatomic) IBOutlet UILabel *postsNumber;
@property (strong, nonatomic) IBOutlet UIButton *goToFollowing;

@end
