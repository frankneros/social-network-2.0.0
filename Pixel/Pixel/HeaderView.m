//
//  HeaderView.m
//  Markt
//
//  Created by Eric Partyka on 5/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "HeaderView.h"

@implementation HeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    self.userProfilePicture.layer.masksToBounds = YES;
    self.userProfilePicture.layer.cornerRadius = 21;
    
    self.usersName.font = [UIFont fontWithName:@"OpenSans" size:16];
}

@end
