//
//  AppearanceWS.h
//  Pixel
//
//  Created by Eric Partyka on 9/26/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppearanceWS : NSObject

+ (void)setAppearanceProxy;

@end
