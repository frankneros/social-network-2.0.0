//
//  SignUp.m
//  Bite
//
//  Created by Eric Partyka on 10/11/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import "SignUp.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "AppDelegate.h"
#import "AFPersioAPIClient.h"
#import "MBProgressHUD.h"
#import "CommonCrypto.h"
#import "CommonDigest.h"
#import "MainViewController.h"
#import "MainTabBar.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "APINetworkController.h"
#import "SSTermsConditions.h"

#define kSalt @"rS24PoJSARl7jCV4ICoimaAeK6fzfoN1"

@interface SignUp () <UIScrollViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIImageView *placeholderImageView;
@property (strong, nonatomic) IBOutlet UIView *theView;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *theScrollView;
@property (strong, nonatomic) NSArray *theArray;
@property (strong, nonatomic) IBOutlet UITextField *first;
@property (strong, nonatomic) IBOutlet UITextField *last;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
@property (strong, nonatomic) NSString *hashedPassword;
@property (strong, nonatomic) IBOutlet UIImageView *userProfilePicture;
@property (strong, nonatomic) IBOutlet UIButton *profilePicButton;

@end

@implementation SignUp
{
    NSUserDefaults *userDefaults;
}

#pragma mark - View Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNavBar];
    [self configureView];
    [self getNotifications];
    
    self.theArray = [[NSArray alloc] init];
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[userDefaults objectForKey:@"USERTOKEN"] isEqualToString:@"TOKENSAVED"])
    {
        NSLog(@"Do Nothing");
    }
    else
    {
        NSString *str = @"1";
        [userDefaults setObject:str forKey:@"USERTOKEN"];
        [userDefaults synchronize];
    }
}

#pragma mark - Private Methods

- (void)configureNavBar
{
     self.navigationItem.title = @"Sign Up!";
}

- (void)configureView
{
    self.placeholderImageView.layer.masksToBounds = YES;
    self.placeholderImageView.layer.cornerRadius = 40;
    self.userProfilePicture.layer.masksToBounds = YES;
    self.userProfilePicture.layer.cornerRadius = 40;
    self.theView.layer.masksToBounds = YES;
    self.theView.layer.cornerRadius = 42.5;
    [self.theScrollView contentSizeToFit];
    
    self.profilePicButton.layer.masksToBounds = YES;
    self.profilePicButton.layer.cornerRadius = 2;
    self.username.layer.masksToBounds = YES;
    self.username.layer.cornerRadius = 2;
    self.password.layer.masksToBounds = YES;
    self.password.layer.cornerRadius = 2;
    self.first.layer.masksToBounds = YES;
    self.first.layer.cornerRadius = 2;
    self.last.layer.masksToBounds = YES;
    self.last.layer.cornerRadius = 2;
    self.email.layer.masksToBounds = YES;
    self.email.layer.cornerRadius = 2;
    self.first.layer.masksToBounds = YES;
    self.first.layer.cornerRadius = 2;
    self.signUpButton.layer.masksToBounds = YES;
    self.signUpButton.layer.cornerRadius = 2;

 
}

- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GetSignUpData"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GoodSignUpFriends"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GoodSignUp"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"BadSignUp"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"InitalSignUp"
                                               object:nil];

}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"GetSignUpData"])
    {
         NSLog (@"Successfully received the test notification!");

        [self performSelector:@selector(getLoginData) withObject:nil afterDelay:2];
    }
    
    if ([[notification name] isEqualToString:@"GoodSignUpFriends"])
    {
        NSDictionary *userInfo = notification.userInfo;
        self.theArray = [userInfo objectForKey:@"goodSignUp"];
        NSString *str = [[self.theArray valueForKeyPath:@"id"] lastObject];
        
        [userDefaults setObject:str forKey:kMyID];
        [userDefaults setObject:@"1" forKey:kLoggedIn];
        [userDefaults synchronize];
        
        [APINetworkController updateInitalFriendsRelationship:[userDefaults objectForKey:kMyID] friendID:[userDefaults objectForKey:kMyID]];
    }
    
    if ([[notification name] isEqualToString:@"InitalSignUp"])
    {
        NSLog (@"Successfully received the test notification!");
        UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
        mainView.selectedIndex = 0;
        [self presentViewController:mainView animated:NO completion:nil];

        
    }
    
    if ([[notification name] isEqualToString:@"BadSignUp"])
    {
        NSLog (@"Successfully received the test notification!");
        
        [userDefaults setObject:@"0" forKey:kLoggedIn];
        [userDefaults synchronize];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Sign Up Failed. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

    
    
}

-(void)getLoginData
{
    NSString* saltedPassword = [NSString stringWithFormat:@"%@%@", _password.text, kSalt];
    NSMutableString* output = [ShortcutsWS secureTheString:saltedPassword];
    [APINetworkController getLoginData:self.username.text password:output];
}

-(void)takePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)choosePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

-(void)finishSignUp
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *mainView =[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    mainView.selectedIndex = 0;
    [self presentViewController:mainView animated:YES completion:nil];
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addPicture:(id)sender
{
    NSString *actionSheetTitle = @"Pick An Option";
    NSString *other1 = @"Take Photo";
    NSString *other2 = @"Choose Photo";
    NSString *cancelTitle = @"Cancel";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    
    [actionSheet showInView:self.view];

}

#pragma mark - Action Sheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if  ([buttonTitle isEqualToString:@"Take Photo"])
    {
        [self takePhoto];
    }
    if ([buttonTitle isEqualToString:@"Choose Photo"])
    {
        [self choosePhoto];
    }
    if ([buttonTitle isEqualToString:@"Cancel"])
    {
        [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
    }
}

- (IBAction)signUp:(id)sender
{
    if ([_username.text isEqualToString:@""] || [_password.text isEqualToString:@""] || [_email.text isEqualToString:@""] || [_first.text isEqualToString:@""] || [_last.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!" message:@"Please fill in all of the fields!"  delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }
    
    if (![ShortcutsWS isEmailValid:self.email.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Email" message:@"Please enter a valid email."  delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        
        return;
    }
    
    else
    {

        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progressHUD.labelText = @"Signing Up";
        progressHUD.mode = MBProgressHUDAnimationFade;
        
        NSString* saltedPassword = [NSString stringWithFormat:@"%@%@", _password.text, kSalt];
        NSMutableString* output = [ShortcutsWS secureTheString:saltedPassword];
        
        UIImage * imageNew = self.userProfilePicture.image;
        CGSize sacleSize = CGSizeMake(95, 95);
        UIGraphicsBeginImageContextWithOptions(sacleSize, NO, 0.0);
        [imageNew drawInRect:CGRectMake(0, 0, sacleSize.width, sacleSize.height)];
        UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImageJPEGRepresentation(self.userProfilePicture.image, 6);
               
        [APINetworkController signUp:self.first.text last:self.last.text username:self.username.text password:output email:self.email.text imageData:imageData token:[userDefaults objectForKey:kToken]];
    
    }
}

- (IBAction)hideKeyboard:(id)sender
{
    [_username endEditing:YES];
    [_password endEditing:YES];
    [_first endEditing:YES];
    [_last endEditing:YES];
    [_email endEditing:YES];
}

- (IBAction)goBack:(id)sender
{
   [self dismissViewControllerAnimated:YES completion:nil]; 
}

- (IBAction)terms:(id)sender
{
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SSTermsConditions *mainView =[mainStoryboard instantiateViewControllerWithIdentifier:@"SSTermsConditions"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mainView];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - Image Picker Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.userProfilePicture.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
