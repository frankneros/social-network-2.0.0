//
//  SettingsDefines.h
//  Pixel
//
//  Created by Eric Partyka on 9/27/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#ifndef Pixel_SettingsDefines_h
#define Pixel_SettingsDefines_h

#define kTempUserID @"kTempUserID"
#define kDidFriend @"kDidFriend"
#define kFollowers @"kFollowers"
#define kFollowing @"kFollowing"
#define kFollowersNew @"kFollowersNew"
#define kFollowingNew @"kFollowingNew"
#define kWalkthroughCompleted @"kWalkthroughCompleted"

#endif
