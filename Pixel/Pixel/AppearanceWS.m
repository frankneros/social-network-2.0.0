//
//  AppearanceWS.m
//  Pixel
//
//  Created by Eric Partyka on 9/26/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "AppearanceWS.h"

@implementation AppearanceWS

+ (void)setAppearanceProxy
{

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:83.0/255.0 green:137.0/255.0 blue:210.0/255.0 alpha:1]];

    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica-Light" size:20], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName,  nil]];
    
    [[UITextField appearance] setKeyboardAppearance:UIKeyboardAppearanceDark];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica-Light" size:20], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName,  nil] forState:UIControlStateNormal];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
   [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:83.0/255.0 green:137.0/255.0 blue:210.0/255.0 alpha:1]];
    
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
}

@end
