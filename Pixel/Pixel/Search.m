//
//  Search.m
//  Markt
//
//  Created by Eric Partyka on 5/28/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "Search.h"
#import "AFNetworking.h"
#import "FindFriendsCell.h"
#import "Settings.h"
#import "MainViewController.h"
#import "APINetworkController.h"
#import "SSUserProfileViewController.h"

@interface Search ()

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UITableView *resultsTableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *theSegment;

@end

@implementation Search
{
    NSArray *results;
    NSUserDefaults *userDefaults;
    NSArray *searchResults;
    NSIndexPath *searchIndexPath;
    FindFriendsCell *cell;
    NSArray *userSettings;
    NSArray* friendIDs;
    NSArray *relationship;
    NSMutableArray *theSearchResults;
    NSArray *hashtags;
    NSArray *hashtagResults;
    
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

-(void)viewDidLayoutSubviews
{
    if (self.searchDisplayController.active)
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [APINetworkController getHashtags];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

     // Do any additional setup after loading the view.
    
    [self configureNavBar];
    [self getNotifications];
    
    theSearchResults = [[NSMutableArray alloc] init];
    searchIndexPath = [[NSIndexPath alloc] init];
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    userSettings = [[NSArray alloc] init];
    results = [[NSArray alloc] init];
    searchResults = [[NSArray alloc] init];
    hashtags = [[NSArray alloc] init];
    hashtagResults = [[NSArray alloc] init];
    relationship = [[NSArray alloc] init];
    
    [APINetworkController getUserList];
    [APINetworkController getUserSettings:[userDefaults objectForKey:kMyID]];
    [APINetworkController getHashtags];
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavBar
{
    self.navigationItem.title = @"Search";
}

- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GetAllUsers"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"UpdateFollowing"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"UpdateFollowers"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshFollow"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshSearchVC"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GotHashtags"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"GetUserListAgain"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshSearchArray"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"UpdateFollowingDelete"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"UpdateFollowersDelete"
                                               object:nil];
 
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"GetAllUsers"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        results = [userInfo objectForKey:@"allUsers"];
        [self.searchDisplayController.searchResultsTableView reloadData];
    }

    
    if ([[notification name] isEqualToString:@"GotHashtags"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        hashtags = [userInfo objectForKey:@"hashtags"];
    }
    
    if ([[notification name] isEqualToString:@"RefreshSearchVC"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        userSettings = [userInfo objectForKey:@"theSearchArray"];
        NSLog(@"User Settings %@", userSettings);
        friendIDs = [userSettings valueForKey:@"friendID"];
        relationship = [userSettings valueForKey:@"friendTableID"];
    
        NSLog(@"Friend ID's %@", friendIDs);
        [self.searchDisplayController.searchResultsTableView reloadData];
     }
    
    if ([[notification name] isEqualToString:@"RefreshFriendArray"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        results = [userInfo objectForKey:@"allUsers"];
        [self.searchDisplayController.searchResultsTableView reloadData];
    }
    
    if ([[notification name] isEqualToString:@"UpdateFollowing"])
    {
        NSLog (@"Successfully received the test notification!");
        
        NSString *searches = [NSString stringWithFormat:@"%@", [userSettings valueForKey:@"following"]];
        NSString *str1 = [searches stringByReplacingOccurrencesOfString:@"(\n" withString:@""];
        NSString *str2 = [str1 stringByReplacingOccurrencesOfString:@"\n)" withString:@""];
        NSString *final = [str2 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSNumber *votes = [NSNumber numberWithFloat:[final floatValue]];
        NSNumber *plusOne = [NSNumber numberWithInt:1];
        NSNumber *addVote = [NSNumber numberWithFloat:([votes floatValue] + [plusOne floatValue])];
        NSString *numbStr = [NSString stringWithFormat:@"%@", addVote];
        NSLog(@"Following Updated %@", numbStr);
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[userDefaults objectForKey:kMyID], @"id", nil];
        
        [APINetworkController updateFollowing:params followingNumber:numbStr];
        [self.searchDisplayController.searchResultsTableView reloadData];
        
    }
    
    if ([[notification name] isEqualToString:@"UpdateFollowers"])
    {
        NSLog (@"Successfully received the test notification!");
        NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
        
        int i = 0;
        NSString *searches = [NSString stringWithFormat:@"%@", [[searchResults valueForKey:@"followers"] objectAtIndex:indexPath.row]];
        NSNumber *votes = [NSNumber numberWithFloat:[searches floatValue]];
        NSNumber *plusOne = [NSNumber numberWithInt:1];
        NSNumber *addVote = [NSNumber numberWithFloat:([votes floatValue] + [plusOne floatValue])];
        NSString *numbStr = [NSString stringWithFormat:@"%@", addVote];
        NSLog(@"Update Followers %@", numbStr);
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[searchResults valueForKey:@"id"] objectAtIndex:indexPath.row], @"id", nil];
        
        [APINetworkController updateFollowers:params followersNumber:numbStr];
        [APINetworkController getUserSettings:[userDefaults objectForKey:kMyID]];
        [self.searchDisplayController.searchResultsTableView reloadData];

    }
    
    if ([[notification name] isEqualToString:@"RefreshFollow"])
    {
        NSLog (@"Successfully received the test notification!");
        [APINetworkController getUserSettings:[userDefaults objectForKey:kMyID]];
        [self.searchDisplayController.searchResultsTableView reloadData];

    }
    
}

-(void)addFriend:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSLog(@"Clicked Cell %ld", (long)btn.tag);
    
    NSDictionary *friendDict = [[searchResults valueForKey:@"id"] objectAtIndex:btn.tag];
    NSString *fStr = [NSString stringWithFormat:@"%@", friendDict];
    
    [APINetworkController addFriend:[userDefaults objectForKey:kMyID] friendID:fStr];
    [self.searchDisplayController.searchResultsTableView reloadData];
    
}

#pragma mark - Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        if (self.theSegment.selectedSegmentIndex == 0) {
            if (searchResults && searchResults.count) {
                return searchResults.count;
            } else {
                return 0;
            }

        }
        
        if (self.theSegment.selectedSegmentIndex == 1) {
            if (hashtagResults && hashtagResults.count) {
                return hashtagResults.count;
            } else {
                return 0;
            }
        }
        
    } else {
        return 0;
    }
    
    return 0;
    
}

#pragma mark - Table View Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
    
        if (self.theSegment.selectedSegmentIndex == 0) {
            cell = (FindFriendsCell *)[self.searchDisplayController.searchResultsTableView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle] loadNibNamed:@"FindFriendsCell" owner:self options:nil] objectAtIndex:0];
                
            }
            
            
            NSMutableDictionary *resultsDict = [searchResults objectAtIndex:indexPath.row];
            NSLog(@"RESULTS %@", resultsDict);
            
            NSString *firstStr = [NSString stringWithFormat:@"%@", [resultsDict objectForKey:@"first"]];
            NSString *lastStr = [NSString stringWithFormat:@"%@", [resultsDict objectForKey:@"last"]];
            NSString *combo = [NSString stringWithFormat:@"%@ %@", firstStr, lastStr];
            cell.nameLabel.text = combo;
            cell.nameLabel.font = [UIFont fontWithName:@"OpenSans" size:15];
            [cell.nameLabel setTextColor:[UIColor darkGrayColor]];
            
            
            NSString *un = [NSString stringWithFormat:@"@%@", [resultsDict objectForKey:@"username"]];
            cell.usernameLabel.text = un;
            cell.usernameLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
            [cell.usernameLabel setTextColor:[UIColor lightGrayColor]];
            
            cell.friendUser.tag = indexPath.row;
            
            if ([resultsDict objectForKey:@"picture"] == nil || [[resultsDict objectForKey:@"picture"] isEqual:[NSNull null]]){
                cell.theImageView.image = [UIImage imageNamed:@"placerholdersquare.png"];
            } else {
                NSURL *url = [[NSURL alloc] initWithString:[resultsDict objectForKey:@"picture"]];
                [cell.theImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placerholdersquare.png"]];
            }
            
            if ([friendIDs containsObject:[resultsDict objectForKey:@"id"]]) {
                cell.friendUser.backgroundColor = [UIColor redColor];

            }
            
            [cell.friendUser addTarget:nil action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.theImageView.layer.masksToBounds = YES;
            cell.theImageView.layer.cornerRadius = 21;
            
            
            return cell;
        }
        
       
        if (self.theSegment.selectedSegmentIndex == 1) {
            static NSString *identifier = @"cell";
            UITableViewCell *hcell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (hcell == nil) {
                hcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            
            NSDictionary *theDict = [hashtagResults objectAtIndex:indexPath.row];
            
            hcell.textLabel.text = [theDict objectForKey:@"hashtag"];
            hcell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
            [hcell.textLabel setTextColor:[UIColor lightGrayColor]];

            
            return hcell;
        }
    }
    
    return 0;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.searchDisplayController.active)
    {
         if (self.theSegment.selectedSegmentIndex == 0)
         {
             NSDictionary *resultsDict = [searchResults objectAtIndex:indexPath.row];
             [userDefaults setObject:[resultsDict objectForKey:@"id"] forKey:kTempUserID];
             [userDefaults synchronize];
             self.tabBarController.selectedIndex = 3;

         }
    }

}

#pragma mark - Search Delegate


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    if (self.theSegment.selectedSegmentIndex == 0) {
        searchResults = [results filteredArrayUsingPredicate:resultPredicate];
        NSLog(@"Search Results %@", searchResults);

    }
    
    if (self.theSegment.selectedSegmentIndex == 1) {
        hashtagResults = [hashtags filteredArrayUsingPredicate:resultPredicate];
        NSLog(@"Hashtag Search Results %@", hashtagResults);
    }
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    [self.searchDisplayController.searchResultsTableView reloadData];
    
    return YES;
}

#pragma mark - Actions

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
