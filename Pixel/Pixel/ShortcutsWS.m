//
//  ShortcutsWS.m
//  Pixel
//
//  Created by Eric Partyka on 9/26/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "ShortcutsWS.h"
#import "CommonCrypto.h"

@implementation ShortcutsWS

+ (NSString *)getCurrentDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:today];
    
    return dateString;
}

+ (NSMutableString *)secureTheString:(NSString *)stringToEncode
{
    const char *cstr = [stringToEncode cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:stringToEncode.length];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, data.length, digest);
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

+ (NSString *)addition:(NSString *)parameter
{
    NSNumber *addPost = [NSNumber numberWithFloat:[parameter floatValue]];
    NSNumber *plusOne = [NSNumber numberWithInt:1];
    NSNumber *add = [NSNumber numberWithFloat:([addPost floatValue] + [plusOne floatValue])];
    NSString *numbStr = [NSString stringWithFormat:@"%@", add];
    
    return numbStr;
}

+ (NSString *)subtraction:(NSString *)parameter
{
    NSNumber *number = [NSNumber numberWithFloat:[parameter floatValue]];
    NSNumber *numberToSub = [NSNumber numberWithInt:1];
    NSNumber *subtract = [NSNumber numberWithFloat:([number floatValue] - [numberToSub floatValue])];
    NSString *numbStr = [NSString stringWithFormat:@"%@", subtract];
    
    return numbStr;
}

+ (NSData *)resizeImage:(NSData *)parameter
{
    UIImage *theImage = [UIImage imageWithData:parameter];
    UIImage * imageNew = theImage;
    CGSize sacleSize = CGSizeMake(95, 95);
    UIGraphicsBeginImageContextWithOptions(sacleSize, NO, 0.0);
    [imageNew drawInRect:CGRectMake(0, 0, sacleSize.width, sacleSize.height)];
    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImageJPEGRepresentation(imageNew, 0.5);
    
    return imageData;

}

+ (BOOL)isEmailValid:(NSString *)email
{
    NSString *laxEmailValidationString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", laxEmailValidationString];
    
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)isValidObject:(NSString *)parameter
{
    if (parameter == nil || [parameter isEqual:[NSNull null]])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

@end
