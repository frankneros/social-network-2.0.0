//
//  SSUserProfileCell.h
//  SocialSell
//
//  Created by Eric Partyka on 8/2/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSUserProfileCell : UICollectionViewCell

#pragma mark - Properties
@property (weak, nonatomic) IBOutlet UIImageView *theImageView;
@property (weak, nonatomic) IBOutlet UILabel *theNameLabel;
@property (weak, nonatomic) IBOutlet UIView *theBackgroundView;

@end
