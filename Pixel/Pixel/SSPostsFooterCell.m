//
//  SSPostsFooterCell.m
//  SocialSell
//
//  Created by Eric Partyka on 7/20/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SSPostsFooterCell.h"

@implementation SSPostsFooterCell
@synthesize optionsButton;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
