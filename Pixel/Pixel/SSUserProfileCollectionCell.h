//
//  SSUserProfileCollectionCell.h
//  SocialSell
//
//  Created by Eric Partyka on 9/24/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSUserProfileCollectionCell : UICollectionViewCell

#pragma mark - Properties
@property (weak, nonatomic) IBOutlet UIImageView *theImageView;
@property (weak, nonatomic) IBOutlet UILabel *theNameLabel;
@property (weak, nonatomic) IBOutlet UIView *theBackgroundView;
@property (strong, nonatomic) IBOutlet UIButton *theBackButton;
@property (strong, nonatomic) IBOutlet UIButton *followButton;
@property (strong, nonatomic) IBOutlet UILabel *followersLabel;
@property (strong, nonatomic) IBOutlet UILabel *followingLabel;

@end
