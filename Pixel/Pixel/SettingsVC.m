//
//  SettingsVC.m
//  Markt
//
//  Created by Eric Partyka on 6/1/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SettingsVC.h"
#import "Login.h"
#import "Settings.h"
#import "SettingsDefines.h"

@interface SettingsVC () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UITableView *theTableView;

@end

@implementation SettingsVC
{
    NSUserDefaults *userDefaults;
}

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self configureNavBar];
    
    [self.theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavBar
{
    self.navigationItem.title = @"Settings";
}

#pragma mark - Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}

#pragma mark - Table View Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    [self configureCell:cell atIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:16];
    [cell.textLabel setTextColor:[UIColor darkGrayColor]];
    
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            cell.textLabel.text = @"Contact Support";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if (indexPath.row == 1)
        {
            cell.textLabel.text = @"Version 1.0.0";
        }
    }
    
    if (indexPath.section == 1)
    {
        cell.textLabel.text = @"Click Here To Logout";
    }

}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"App Settings & Support";
    }
    else
    {
        return @"Logout";
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0)
        {
            NSString *emailTitle = @"App Support";
            NSString *messageBody = @"Please enter your message here for app support.";
            NSArray *toRecipents = [NSArray arrayWithObject:@"support@myapp.com"];
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            
            mc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                    [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"OpenSans" size:16], NSFontAttributeName, nil];
            
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [[mc navigationBar] setTintColor:[UIColor whiteColor]];
                
            }
            else
            {
                mc.navigationController.navigationBar.TintColor = [UIColor colorWithRed:97.0/255.0 green:183.0/255.0 blue:242.0/255.0 alpha:1];
                mc.navigationController.navigationBar.translucent = NO;
                
            }
            [self.navigationController presentViewController:mc animated:YES completion:nil];

        }
    }
    
    else
    {
        [userDefaults setObject:@"0" forKey:kLoggedIn];
        [userDefaults synchronize];
        UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        Login *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Login"];
        [self presentViewController:detailVC animated:NO completion:nil];
        
    }
}

#pragma mark - Mail Composer Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Actions

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
