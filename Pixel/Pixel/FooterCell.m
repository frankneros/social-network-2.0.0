//
//  FooterCell.m
//  Markt
//
//  Created by Eric Partyka on 5/31/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "FooterCell.h"

@implementation FooterCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
