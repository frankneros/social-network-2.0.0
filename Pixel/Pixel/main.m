//
//  main.m
//  Bite
//
//  Created by Eric Partyka on 10/2/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
