//
//  ViewController.m
//  SidebarDemo
//
//  Created by Simon on 28/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "PictureCell.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "AFPersioAPIClient.h"
#import "MBProgressHUD.h"
#import "SSPullToRefresh.h"
#import "Login.h"
#import "AppSettings.h"
#import "SWRevealViewController.h"
#import "HeaderView.h"
#import "AdddPicture.h"
#import "MainTabBar.h"
#import "FooterView.h"
#import "UIViewController+ScrollingNavbar.h"
#import "CommentsVC.h"
#import "FooterCell.h"
#import "MovieCell.h"
#import "APINetworkController.h"
#import "NSDate+NVTimeAgo.h"
#import "SSPostsFooterCell.h"
#import "SSUserProfileViewController.h"
#import "SDWTwitterLabel.h"

@interface MainViewController ()

#pragma mark - Properties
@property (strong, nonatomic) AMAttributedHighlightLabel *tweetLabel;
@property (strong, nonatomic) SSPullToRefreshView *pullToRefreshView;
@property (strong, nonatomic) IBOutlet UITableView *postsTable;

@end

@implementation MainViewController
{
    NSUserDefaults *userDefaults;
    NSArray *results;
    NSMutableIndexSet *selected;
    NSIndexPath *theIndexPath;
    NSString *postID;
    MainTabBar *theMainTabBar;
    NSArray *likesArray;
    MovieCell *mcell;
    NSString *tempPostID;
}

#pragma mark - View Lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    [APINetworkController getPosts:[userDefaults objectForKey:kMyID]];
}

- (void)viewWillAppear:(BOOL)animated
{
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    footer.backgroundColor = [UIColor clearColor];
    self.postsTable.tableFooterView = footer;
    
    [self showNavBarAnimated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.title = @"SociallyMe";
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    userDefaults = [[NSUserDefaults alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    results = [[NSArray alloc] init];
    likesArray = [[NSArray alloc] init];

    [self configureView];
    [self getNotifications];
    
    [self.postsTable registerNib:[UINib nibWithNibName:@"SSPostsFooterCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SSPostsFooterCell"];
    [self.postsTable registerNib:[UINib nibWithNibName:@"PictureCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PictureCell"];
    [self.postsTable registerNib:[UINib nibWithNibName:@"MovieCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MovieCell"];
    [self.postsTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
   
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"StopMoviePlayer" object:nil userInfo:nil];
}

#pragma mark - Private Methods

- (void)configureView
{
    userDefaults = [NSUserDefaults standardUserDefaults];
    userDefaults = [[NSUserDefaults alloc] init];
    theIndexPath = [[NSIndexPath alloc] init];
    self.postsTable.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.pullToRefreshView = [[SSPullToRefreshView alloc] initWithScrollView:self.postsTable delegate:self];
}


- (void)getNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshPosts"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RefreshLikes"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"ReloadPostsTable"
                                               object:nil];

}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"RefreshPosts"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        results = [userInfo objectForKey:@"postsArray"];
        
        if ([results count] > 2)
        {
            [self followScrollView:self.postsTable];
        }
        
        [APINetworkController getLikes:[userDefaults objectForKey:kMyID]];
        [self.postsTable reloadData];
        
    }
    
    if ([[notification name] isEqualToString:@"ReloadPostsTable"])
    {
        NSLog (@"Successfully received the test notification!");
        UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainTabBar *main = [mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
        [self presentViewController:main animated:NO completion:nil];
        
    }
    
    if ([[notification name] isEqualToString:@"RefreshLikes"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        likesArray = [userInfo objectForKey:@"likesArray"];
        [self.postsTable reloadData];
    }
    
    if ([[notification name] isEqualToString:@"StopMoviePlayer"])
    {
        NSLog (@"Successfully received the test notification!");
        NSDictionary *userInfo = notification.userInfo;
        MovieCell *cell = [userInfo objectForKey:@"movieCell"];
        [cell.moviePlayer stop];
    }

    
}

-(void)bringUpOptions:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSDictionary *theDict = [results objectAtIndex:btn.tag];
    tempPostID = [theDict objectForKey:@"postID"];
    
    if ([[theDict objectForKey:@"userID"] isEqualToString:[userDefaults objectForKey:kMyID]])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Choose" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
        [alert show];
        [userDefaults setObject:[theDict objectForKey:@"id"] forKey:@"deleteID"];
        [userDefaults synchronize];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Post" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes Report", nil];
        [alert show];
    }
}

-(void)deletePost
{
    NSString *tempPostID = [userDefaults objectForKey:@"tPostID"];
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:tempPostID, @"id", nil];
    [APINetworkController deletePost:params];
}


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (results && results.count) {
        return results.count;
    } else {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

#pragma mark - Table View Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *resultsDict = [results objectAtIndex:indexPath.section];
    self.postsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    if (indexPath.row == 0)
    {
        if ([[resultsDict objectForKey:@"isImage"] isEqualToString:@"1"])
        {
             [[NSNotificationCenter defaultCenter] postNotificationName: @"StopMoviePlayer" object:nil userInfo:nil];
            PictureCell *cell = (PictureCell *)[tableView dequeueReusableCellWithIdentifier:@"PictureCell"];
            [self configurePictureCell:cell atIndexPath:indexPath];
            
            return cell;
        }
        else if ([[resultsDict objectForKey:@"isImage"] isEqualToString:@"0"])
        {
            MovieCell *cell = (MovieCell *)[tableView dequeueReusableCellWithIdentifier:@"MovieCell"];
            [self configureMovieCell:cell atIndexPath:indexPath];
            return cell;
        }

    }
    
    if (indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
        [self configureNormalCell:cell atIndexPath:indexPath];
        
        return cell;

    }
    
    
    if (indexPath.row == 2) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"StopMoviePlayer" object:nil userInfo:nil];
        SSPostsFooterCell *cell = (SSPostsFooterCell *)[tableView dequeueReusableCellWithIdentifier:@"SSPostsFooterCell"];
        cell.likeButton.tag = indexPath.section;
        cell.commentsButton.tag = indexPath.section;
        [self configureFooterCell:cell atIndexPath:indexPath];
        
        return cell;
        
    }

    return 0;
}

- (void)configureNormalCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *theDict = [results objectAtIndex:indexPath.section];
    
    if ([ShortcutsWS isValidObject:[theDict objectForKey:@"description"]])
    {

        UIFont *cellFont = AppLightFontOfSize(14);
        
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:[theDict objectForKey:@"description"] attributes:@{NSFontAttributeName: cellFont}];
        CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(self.postsTable.bounds.size.width, CGFLOAT_MAX)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        
        CGFloat theHeight = rect.size.height + 85;

        
        if (!self.tweetLabel)
        {
            self.tweetLabel = [[AMAttributedHighlightLabel alloc] initWithFrame:CGRectMake(20, 1, 300, theHeight)];
            self.tweetLabel.delegate = self;
            self.tweetLabel.hashtagTextColor = [UIColor colorWithRed:83.0/255.0 green:137.0/255.0 blue:210.0/255.0 alpha:1];
            self.tweetLabel.userInteractionEnabled = YES;
            self.tweetLabel.numberOfLines = 0;
            self.tweetLabel.lineBreakMode = NSLineBreakByWordWrapping;
            self.tweetLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:16];

        }
        
       
        [self.tweetLabel setString:[theDict objectForKey:@"description"]];
        self.tweetLabel.hashtagTextColor = [UIColor colorWithRed:83.0/255.0 green:137.0/255.0 blue:210.0/255.0 alpha:1];
        [cell addSubview:self.tweetLabel];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
}

-(void)configurePictureCell:(PictureCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *theDict = [results objectAtIndex:indexPath.section];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"StopMoviePlayer" object:nil userInfo:nil];
    
    if ([ShortcutsWS isValidObject:[theDict objectForKey:@"postURL"]])
    {
        if ([[theDict objectForKey:@"isImage"] isEqualToString:@"1"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName: @"StopMoviePlayer" object:nil userInfo:nil];
            
            cell.postedPicture.alpha = 0.0;
            
            [UIView animateWithDuration:.75 animations:^{
                
                cell.postedPicture.alpha = 1.0;
                NSURL *url = [[NSURL alloc] initWithString:[theDict objectForKey:@"postURL"]];
                [cell.postedPicture setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placerholdersquare.png"]];
                NSString *tPostID = [theDict objectForKey:@"id"];
                [userDefaults setObject:tPostID forKey:@"tPostID"];
                [userDefaults synchronize];
                
            }];

        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

}

- (void)configureMovieCell:(MovieCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *theDict = [results objectAtIndex:indexPath.section];
    
    if ([ShortcutsWS isValidObject:[theDict objectForKey:@"postURL"]])
    {
        cell.moviePlayer.controlStyle = MPMovieControlStyleNone;
        cell.moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
        cell.moviePlayer.repeatMode = MPMovieRepeatModeOne;
        cell.moviePlayer.view.frame = CGRectMake(0, 0, 320, 300);
        [cell.contentView addSubview:cell.moviePlayer.view];
        NSString *filepath = [NSString stringWithFormat:@"%@", [theDict objectForKey:@"postURL"]];
        [cell.moviePlayer setContentURL:[NSURL URLWithString:filepath]];
        [cell.moviePlayer prepareToPlay];
        [cell.moviePlayer play];
        [cell.moviePlayer shouldAutoplay];
        NSString *tPostID = [theDict objectForKey:@"id"];
        [userDefaults setObject:tPostID forKey:@"tPostID"];
        [userDefaults synchronize];

    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 }


-(void)configureFooterCell:(SSPostsFooterCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *theDict = [results objectAtIndex:indexPath.section];
    NSLog(@"XXXXX %@", likesArray);
    
    if ([likesArray count] > 0)
    {
        if (![[likesArray valueForKey:@"likedPostID"] containsObject:[theDict objectForKey:@"postID"]])
        {
            cell.likeButton.userInteractionEnabled = YES;
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else
        {
            cell.likeButton.userInteractionEnabled = NO;
        }
    }
    else
    {
       [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
    }
    
   
   
    if (![[theDict objectForKey:@"likes"] isEqualToString:@"0"])
    {
        NSString *str = [NSString stringWithFormat:@"%@ Likes", [theDict objectForKey:@"likes"]];
        [cell.likeButton setTitle:str forState:UIControlStateNormal];
    }
    else
    {
        [cell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
    }

    cell.likeButton.layer.masksToBounds = YES;
    cell.likeButton.layer.cornerRadius = 2;
    cell.commentsButton.layer.masksToBounds = YES;
    cell.commentsButton.layer.cornerRadius = 2;
    
    [cell.optionsButton addTarget:self action:@selector(bringUpOptions:) forControlEvents:UIControlEventTouchUpInside];
    [cell.commentsButton addTarget:self action:@selector(goToComments:) forControlEvents:UIControlEventTouchUpInside];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ([results count] > 0)
    {
        UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
        footer.backgroundColor = [UIColor clearColor];
        return footer;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([results count] > 0)
    {
        return 14;
    }
    else
    {
        return 0;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSBundle* mainBundle = [NSBundle mainBundle];
    NSArray* arrayOfViews = [mainBundle loadNibNamed:@"HeaderView" owner:nil options:nil];
    HeaderView *rootView = [arrayOfViews objectAtIndex:0];
    
    NSDictionary *theDict = [results objectAtIndex:section];
    
    if ([ShortcutsWS isValidObject:[theDict objectForKey:@"picture"]])
    {
        NSURL *url = [[NSURL alloc] initWithString:[theDict objectForKey:@"picture"]];
        [rootView.userProfilePicture setImageWithURL:url placeholderImage:[UIImage imageNamed:@"userplaceholder.png"]];
        
        NSString *mysqlDatetime = [theDict objectForKey:@"theDate"];
        NSString *timeAgoFormattedDate = [NSDate mysqlDatetimeFormattedAsTimeAgo:mysqlDatetime];
        rootView.theDate.text = timeAgoFormattedDate;
        rootView.theDate.font = AppLightFontOfSize(9);
        
        NSString *firstStr = [NSString stringWithFormat:@"%@", [theDict objectForKey:@"first"]];
        NSString *lastStr = [NSString stringWithFormat:@"%@", [theDict objectForKey:@"last"]];
        NSString *combo = [NSString stringWithFormat:@"%@ %@", firstStr, lastStr];
        [rootView.theUsername setTitle:[NSString stringWithFormat:@"@%@", [theDict objectForKey:@"username"]] forState:UIControlStateNormal];
        [rootView.theUsername addTarget:self action:@selector(goToUserProfile:) forControlEvents:UIControlEventTouchUpInside];
        [rootView.theImageButton addTarget:self action:@selector(goToUserProfile:) forControlEvents:UIControlEventTouchUpInside];
        rootView.theUsername.tag = section;
        rootView.theImageButton.tag = section;
        
        

    }
    return rootView;
}

- (void)goToUserProfile:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    NSDictionary *theDict = [results objectAtIndex:btn.tag];
    [userDefaults setObject:[theDict objectForKey:@"userID"] forKey:kTempUserID];
    [userDefaults synchronize];
    self.tabBarController.selectedIndex = 3;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
       
        MovieCell *cell = (MovieCell *)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
            [cell.moviePlayer play];
        } else if (cell.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
            [cell.moviePlayer pause];
        }
    }
    
}

- (void)likePost:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSDictionary *theDict = [results objectAtIndex:button.tag];
    
    if ([likesArray count] > 0)
    {
        NSString *postIDV = [NSString stringWithFormat:@"%@", [[results valueForKey:@"postID"] objectAtIndex:button.tag]];
        
        NSString *str = [ShortcutsWS addition:[[results valueForKeyPath:@"likes"] lastObject]];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:postIDV, @"id", nil];
        
        [APINetworkController likePostAfterCountOne:params likeNumber:str];
        [APINetworkController addLikesToPostsTable:[userDefaults objectForKey:kMyID] postID:postIDV];
        [APINetworkController getPosts:[userDefaults objectForKey:kMyID]];
        [APINetworkController getLikes:[userDefaults objectForKey:kMyID]];    }
    else
    {
        NSString *str = [ShortcutsWS addition:[[results valueForKeyPath:@"likes"] lastObject]];
        NSString *postIDV = [NSString stringWithFormat:@"%@", [[results valueForKey:@"postID"] objectAtIndex:button.tag]];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:postIDV, @"id", nil];
        
        [APINetworkController addInitialLikes:params likeNumber:str];
        [APINetworkController addLikesToPostsTable:[userDefaults objectForKey:kMyID] postID:postIDV];
        [APINetworkController getPosts:[userDefaults objectForKey:kMyID]];
        [APINetworkController getLikes:[userDefaults objectForKey:kMyID]];
    }
    
}

- (void)goToComments:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSDictionary *theDict = [results objectAtIndex:button.tag];
    
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CommentsVC *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"CommentsVC"];
    mainView.commentID = [theDict objectForKey:@"postID"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mainView];
    [self presentViewController:nav animated:NO completion:nil];

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 48;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 300;
    }
    else if (indexPath.row == 1)
    {
        NSDictionary *theDict = [results objectAtIndex:indexPath.section];
        
        if ([[theDict objectForKey:@"description"] length] > 0)
        {
            
            UIFont *cellFont = AppLightFontOfSize(14);
            
            NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:[theDict objectForKey:@"description"] attributes:@{NSFontAttributeName: cellFont}];
            CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            
            return rect.size.height + 85;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 50;
    }
    
}


#pragma mark - Alert View Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Delete"])
    {
        [self deletePost];
    }
    
    if ([title isEqualToString:@"Yes Report"])
    {
        [APINetworkController reportPost:[userDefaults objectForKey:kMyID] postID:tempPostID];
    }
}

#pragma mark - Hashtag Arguments
- (void)selectedMention:(NSString *)string
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}
- (void)selectedHashtag:(NSString *)string
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}
- (void)selectedLink:(NSString *)string
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Selected" message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

#pragma mark -  Pull To Refresh Arguments

- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view
{
    [self.pullToRefreshView startLoading];
    UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainTabBar *main = [mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
    [self presentViewController:main animated:YES completion:nil];
    [self.pullToRefreshView finishLoading];
}

@end
