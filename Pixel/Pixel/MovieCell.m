//
//  MovieCell.m
//  Markt
//
//  Created by Eric Partyka on 6/14/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "MovieCell.h"

@implementation MovieCell

- (void)awakeFromNib
{
    // Initialization code
    self.moviePlayer = [[MPMoviePlayerController alloc] init];
    [self addSubview:self.moviePlayer.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"StopMoviePlayer"
                                               object:nil];

}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"StopMoviePlayer"])
    {
        NSLog (@"Successfully received the test notification!");
        [self.moviePlayer stop];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
