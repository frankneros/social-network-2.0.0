//
//  FooterView.h
//  Markt
//
//  Created by Eric Partyka on 5/30/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterView : UIView

#pragma mark - Properties
@property (strong, nonatomic) IBOutlet UIButton *leaveComment;
@property (strong, nonatomic) IBOutlet UIButton *favoritePost;

@end
