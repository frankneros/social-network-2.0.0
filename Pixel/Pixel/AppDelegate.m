//
//  AppDelegate.m
//  Bite
//
//  Created by Eric Partyka on 10/2/13.
//  Copyright (c) 2013 Eric Partyka. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFPersioAPIClient.h"
#import "Settings.h"
#import "SWRevealViewController.h"
#import "Login.h"
#import "AppearanceWS.h"
#import "SSWalkthroughViewController.h"
#import "MainTabBar.h"
#import "SettingsDefines.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:(v) options:NSNumericSearch] != NSOrderedAscending)

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
    else
    {
    
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
    }
    
    
    [self.window.layer setCornerRadius:5.0f];
    [self.window.layer setMasksToBounds:YES];
    
    [AppearanceWS setAppearanceProxy];
    
    
    UIStoryboard *mainStoryboard = nil;
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    self.userDefaults = [[NSUserDefaults alloc] init];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *str = [self.userDefaults objectForKey:kWalkthroughCompleted];
    
    if ([str isEqualToString:@"1"]) {
        if ([[self.userDefaults objectForKey:kLoggedIn] isEqualToString:@"1"])
        {
            UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MainTabBar *mainView=[mainStoryboard instantiateViewControllerWithIdentifier:@"tabBar"];
            self.window.rootViewController = mainView;
        }
        else
        {
            UIStoryboard *mainStoryboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            Login *detailVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Login"];
            [self.window.rootViewController presentViewController:detailVC animated:NO completion:nil];
        }
    }
    else
    {
        SSWalkthroughViewController *walkthroughVC = [[SSWalkthroughViewController alloc] initWithNibName:@"SSWalkthroughViewController" bundle:[NSBundle mainBundle]];
        self.window.rootViewController = walkthroughVC;
    }
    return YES;
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{

    NSString *tokenStr = [NSString stringWithFormat:@"%@", deviceToken];
    NSString *lessThan = [tokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *greaterThan = [lessThan stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSString *noSpacesStr = [greaterThan stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"No Spaces String %@", noSpacesStr);
    
    [self.userDefaults setObject:noSpacesStr forKey:kToken];
    [self.userDefaults synchronize];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    UIApplicationState state = [application applicationState];
    
    
    if (state == UIApplicationStateActive)
    {
        
    }
    else
    {
    
    }
}



@end
