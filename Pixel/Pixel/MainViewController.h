//
//  ViewController.h
//  SidebarDemo
//
// 
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SSPullToRefresh.h"
#import <MediaPlayer/MediaPlayer.h>
#import "STTweetLabel.h"
#import "AMAttributedHighlightLabel.h"

@interface MainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SSPullToRefreshViewDelegate, UIAlertViewDelegate, AMAttributedHighlightLabelDelegate>
{
    MPMoviePlayerController *moviePlayer;
}

@end
