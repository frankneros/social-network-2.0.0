//
//  AppSettings.m
//  Bite
//
//  Created by Eric Partyka on 1/18/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "AppSettings.h"
#import "MainViewController.h"

@interface AppSettings () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@end

@implementation AppSettings

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavBar
{
   self.navigationItem.title = @"Settings";
}

#pragma mark - Table View Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

#pragma mark - Table View Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    switch (indexPath.section) {
       case 0:
        {
            cell.textLabel.text = @"support@themarktapp.com";
            cell.detailTextLabel.text = @"http://wwww.themarktapp.com";
            
            cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:16];
            [cell.textLabel setTextColor:[UIColor darkGrayColor]];
            
            cell.detailTextLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:14];
            [cell.detailTextLabel setTextColor:[UIColor darkGrayColor]];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        }
            break;
        case 2:
        {
            cell.textLabel.text = @"1.0";
            
            cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:16];
            [cell.textLabel setTextColor:[UIColor darkGrayColor]];
    
        }
            break;
        default:
            break;
    }
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return @"Contact Markt";
        }
            break;
        case 1:
        {
            return @"Build";
        }
            break;
        default:
            break;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSString *emailTitle = @"Markt Support";
        NSString *messageBody = @"Please enter your message here for Martk support.";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"support@themarktapp.com"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        mc.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"OpenSans" size:16], NSFontAttributeName, nil];
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [[mc navigationBar] setTintColor:[UIColor whiteColor]];
            
        } else {
            mc.navigationController.navigationBar.TintColor = [UIColor colorWithRed:97.0/255.0 green:183.0/255.0 blue:242.0/255.0 alpha:1];
            mc.navigationController.navigationBar.translucent = NO;

        }
        // Present mail view controller on screen
        [self.navigationController presentViewController:mc animated:YES completion:nil];
        
    }
}

#pragma mark - Mail Composer Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Actions

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

