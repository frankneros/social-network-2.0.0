//
//  SSTermsConditions.m
//  SocialSell
//
//  Created by Eric Partyka on 8/27/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import "SSTermsConditions.h"
#import "APIHelper.h"

@interface SSTermsConditions ()

#pragma mark - Properties
@property (nonatomic, weak) IBOutlet UIWebView *theWebView;

#pragma mark - Actions
- (IBAction)goBack:(id)sender;

@end

@implementation SSTermsConditions

#pragma mark - Instance Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    
    [self configureNavBar];
    
    NSString *str = [APIHelper baseURL];
    NSString *strURL = [NSString stringWithFormat:@"%@%@", str, @"terms.docx"];
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.theWebView loadRequest:request];
    
    // Do any additional setup after loading the view.
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)configureNavBar
{
    self.navigationItem.title = @"Terms & Conditions";
}

#pragma mark - Actions

- (IBAction)goBack:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
