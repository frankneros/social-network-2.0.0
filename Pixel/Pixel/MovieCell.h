//
//  MovieCell.h
//  Markt
//
//  Created by Eric Partyka on 6/14/14.
//  Copyright (c) 2014 Eric Partyka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MovieCell : UITableViewCell

#pragma mark - Properties
@property (strong, nonatomic)  MPMoviePlayerController *moviePlayer;

@end
